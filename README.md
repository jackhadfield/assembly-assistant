## Assembly Guidance by a Robot

To install dependencies and compile, run the following:

```bash
git clone https://jackhadfield@bitbucket.org/jackhadfield/assembly-assistant.git
cd assembly-assistant
#install dependecies
sudo apt-get update
sudo apt-get -y install libglew-dev libglm-dev libsdl2-dev libsdl2-image-dev libcgal-dev libcgal-qt5-dev
catkin_make -DCMAKE_BUILD_TYPE=Release -DDBOT_BUILD_GPU=Off
source devel/setup.bash
```

The file _src/robot-assembly-assistant/zenoGestures.py_ should be altered to make Zeno perform the desired commands (see _src/robot-assembly-assistant/naoGestures.py_). Also change the camera-to-robot transformation matrix _self.camera2Robot_ in _src/robot-assembly-assistant/robot\_ros\_bridge.py_, according to setup. We don't perform robot tracking, so this must be done manually (the robot and camera coordinate frames shouldn't change during the experiment).

## Setup

The depth device should face the table from the front, while the robot should stand to the side, facing the table (see image from deliverable 5.4). The child should sit opposite the depth camera, so as to limit occlusions.

## Configuration files

Change the topics in the following files, according to the depth device used:

* _colour\_based\_tracker/config/colours(taskX).yaml_ : camera_topic, rgb_camera_info_topic
* _object\_assembly\_ros/config/background\_removal.yaml_ : depth_image_topic, point_cloud_topic, camera_info_topic
* _object\_assembly\_ros/config/image\_crop.yaml_ : image_topic, camera_info_topic
* _object\_assembly\_ros/config/pose\_initializer(taskX).yaml_ : frame_id

Please use the registered color and depth streams, and preferably SD over HD if available.

## Steps

1. `roslaunch object_assembly_ros assembly_experiment_taskX.launch`
2. Record topics to a .bag file (see below).
3. Press 'c' on Image Crop window and select to crop. Crop so that only the tabletop and the bricks are visible to reduce background artifacts. The crop settings will then be saved to a temporary file (specified in _src/object\_assembly\_ros/image\_crop.yaml_). You can then press 'l' on subsequent runs to restore the last selected crop range.
4. If selecting colors online: Select regions on Color Image window, that contain the bricks (only pixels belonging to the bricks, not a bounding box). In order: green then blue for task1, red then green for task2, red then green then blue for task3. Only select one from each color.
5. Thats it! When the robot greets the child he/she can begin.

Note: In _dbot\_ros/config/particle\_tracker.yaml_ you can change the number of particles used by the tracker, as well as the number of threads. If the number of particles is too high, the tracker will print a message to the console for each image not processed. In such cases it's best to reduce the particles (it doesn't matter if a few images aren't processed, though). The number of threads should be chosen so as to maximize the allowed number of particles.

## Recording

Please record the following ROS topics:

* depth (device-specific)
* color (device-specific)
* camera_info (device-specific)
* tf
* /pose\_initializer/inital\_poses
* /image\_crop/crop\_range
* /image\_crop/newCameraInfo
* /particle\_tracker/objects\_state
* /assembly\_agent/connection\_list
* /assembly\_agent/connection\_vector
* /assembly\_agent/connection\_vector\_string
* /colour\_based\_tracker/HSV\_ranges
* /robot_response

