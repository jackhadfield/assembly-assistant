#ifndef SELECTOR_H
#define SELECTOR_H
/**
 * @copyright
 *
 * Copyright 2012 Kevin Schluff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2, 
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 */

#include <opencv2/opencv.hpp>
#include <vector>
#include <cmath>

using namespace cv;

class Selector
{
public:

    Selector(const char* window)
      :m_selection_valid(false),
       m_selecting(false),
       m_selection(),
       m_origin()
    {
      cv::setMouseCallback(window, mouse_callback, this);
    }

    Selector()
      :m_selection_valid(false),
       m_selecting(false),
       m_selection(),
       m_origin()
    {
    }

    bool valid() const
    { return m_selection_valid; }

    bool selecting() const
    { return m_selecting; }

    const cv::Rect& selection() const
    { return m_selection; }

    bool selections(int num_objects, std::vector<cv::Rect>& output)
    {
        if (num_objects < m_selections.size())
            return false;
        else
        {
            output = m_selections;
            return true;
        }
    }

    int calculate_ranges(Mat HSV, std::vector<Mat>& means, std::vector<Mat>& stds, Mat& mask)
    {
        for (int i_range = num_ranges_calculated_; i_range < m_selections.size(); i_range++)
        {
            Mat mean, std;
            meanStdDev(HSV(m_selections[i_range]), mean, std, mask(m_selections[i_range]));
            std::vector<double> angles;
            for (int i = 0; i < HSV(m_selections[i_range]).rows; ++i)
                for (int j = 0; j < HSV(m_selections[i_range]).cols; ++j)
                    if (HSV(m_selections[i_range]).at<Vec3b>(i, j)[0] || 
                        HSV(m_selections[i_range]).at<Vec3b>(i, j)[1] ||
                        HSV(m_selections[i_range]).at<Vec3b>(i, j)[2])
                            angles.push_back(2*HSV(m_selections[i_range]).at<Vec3b>(i, j)[0]);
            mean.at<double>(0,0) = meanAngle(angles.data(), angles.size())/2;
            std.at<double>(0,0) = stdAngle(angles.data(), angles.size())/2;
            means_.push_back(mean);
            stds_.push_back(std);
        }
        num_ranges_calculated_ = m_selections.size();
        means = means_;
        stds = stds_;
        return num_ranges_calculated_;
    }

private:
    static void mouse_callback(int event, int x, int y, int flags, void* data);

   
    double meanAngle(double *angles, int size)
    {
        double y_part = 0, x_part = 0;

        for (int i = 0; i < size; i++)
        {
            x_part += std::cos(angles[i] * M_PI / 180);
            y_part += std::sin(angles[i] * M_PI / 180);
        }
        //TODO 0 check
        double result = std::atan2(y_part / size, x_part / size) * 180 / M_PI;
        return (result >= 0) ? result : result + 360;
    }

    double stdAngle(double *angles, int size)
    {
        double sin = 0;
        double cos = 0;
        for (int i = 0; i < size; i++)
        {
            sin += std::sin(angles[i] * (M_PI/180.0));
            cos += std::cos(angles[i] * (M_PI/180.0)); 
        }
        sin /= size;
        cos /= size;

        double stddev = std::sqrt(1-std::sqrt(sin*sin+cos*cos))*360/std::sqrt(2);
        //double stddev = std::sqrt(-std::log(sin*sin+cos*cos));

        return stddev;
     }

public:
    int num_ranges_calculated_ = 0;
    std::vector<Mat> means_;
    std::vector<Mat> stds_;
    bool m_selection_valid;
    bool m_selecting;
    cv::Rect m_selection;
    std::vector<cv::Rect> m_selections;
    cv::Point m_origin;
};

void Selector::mouse_callback(int event, int x, int y, int flags, void* data)
{
    Selector& self = *((Selector*)data);

    switch( event )
    {
    case CV_EVENT_LBUTTONDOWN:
        self.m_selection_valid = false;
        self.m_selecting = true;
        self.m_selection = Rect(0,0,0,0);
        self.m_origin.x = x;
        self.m_origin.y = y;
        break;
    case CV_EVENT_LBUTTONUP:
        self.m_selection_valid = true;
        self.m_selecting = false;
        self.m_selections.push_back(self.m_selection);
    default:
        if( self.m_selecting )
        {
            self.m_selection.x = MIN(x, self.m_origin.x);
            self.m_selection.y = MIN(y, self.m_origin.y);
            self.m_selection.width = std::abs(x - self.m_origin.x);
            self.m_selection.height = std::abs(y - self.m_origin.y);
        }
    }
}

#endif
