/*
 * This file is part of Assembly Assistant,
 * (https://bitbucket.org/jackhadfield/assembly-assistant)
 *
 * Copyright 2018, Jack Hadfield
 *
 * Assembly Assistant is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Assembly Assistant is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Assembly Assistant.  If not, see <http://www.gnu.org/licenses/>.
 */

//TODO Tidy up!!!

#include "myColourTracker.hpp"
#include <std_msgs/Int32MultiArray.h>
#include "hungarian.hpp"
#include <algorithm>

template <typename T>
vector<size_t> sort_indexes(const vector<T> &v) {
  // initialize original index locations
  vector<size_t> idx(v.size());
  iota(idx.begin(), idx.end(), 0);

  // sort indexes based on comparing values in v
  sort(idx.begin(), idx.end(),
       [&v](size_t i1, size_t i2) {return v[i1] > v[i2];});

  return idx;
}

ThingToFind::ThingToFind()
{
    for (int i=0;i<6;i++)
        values[i] = 0;
    name = "";
}

ThingToFind::ThingToFind(std::vector<int> val, std::string strname)
{
    for (int i=0;i<6;i++)
        values[i] = val[i];
    name = strname;
}

ThingToFind::~ThingToFind()
{
}

//initial min and max HSV filter values.
//these will be changed using trackbars
int H_MIN = 0;
int H_MAX = 180;
int S_MIN = 0;
int S_MAX = 256;
int V_MIN = 0;
int V_MAX = 256;


std::string intToString(int number) {
    	std::stringstream ss;
    	ss << number;
    	return ss.str();
}

void drawObject(int x, int y, Mat &frame, std::string objectName) {
	//use some of the openCV drawing functions to draw crosshairs
	//on your tracked image!

    Scalar colour;

    if (objectName=="Green Box")
        colour = Scalar(0, 192, 0);
    if (objectName=="Red Box")
        colour = Scalar(0, 0, 255);
    if (objectName=="Blue Box")
        colour = Scalar(255, 128, 0);
    if (objectName=="Yellow Box")
        colour = Scalar(0, 255, 255);
    if (objectName=="Purple Box")
        colour = Scalar(255, 0, 128);

	circle(frame, Point(x, y), 20, colour, 2);
	if (y - 25>0)
		line(frame, Point(x, y), Point(x, y - 25), colour, 2);
	else line(frame, Point(x, y), Point(x, 0), colour, 2);
	if (y + 25<FRAME_HEIGHT)
		line(frame, Point(x, y), Point(x, y + 25), colour, 2);
	else line(frame, Point(x, y), Point(x, FRAME_HEIGHT), colour, 2);
	if (x - 25>0)
		line(frame, Point(x, y), Point(x - 25, y), colour, 2);
	else line(frame, Point(x, y), Point(0, y), colour, 2);
	if (x + 25<FRAME_WIDTH)
		line(frame, Point(x, y), Point(x + 25, y), colour, 2);
	else line(frame, Point(x, y), Point(FRAME_WIDTH, y), colour, 2);

	putText(frame, objectName + intToString(x) + "," + intToString(y), Point(x, y + 30), 1, 1, colour, 2);

}

void drawObject(int x, int y, Mat &frame, std::string objectName, Scalar colour) {
	//use some of the openCV drawing functions to draw crosshairs
	//on your tracked image!

	circle(frame, Point(x, y), 20, colour, 2);

	if (y - 25>0)
		line(frame, Point(x, y), Point(x, y - 25), colour, 2);
	else line(frame, Point(x, y), Point(x, 0), colour, 2);
	if (y + 25<FRAME_HEIGHT)
		line(frame, Point(x, y), Point(x, y + 25), colour, 2);
	else line(frame, Point(x, y), Point(x, FRAME_HEIGHT), colour, 2);
	if (x - 25>0)
		line(frame, Point(x, y), Point(x - 25, y), colour, 2);
	else line(frame, Point(x, y), Point(0, y), colour, 2);
	if (x + 25<FRAME_WIDTH)
		line(frame, Point(x, y), Point(x + 25, y), colour, 2);
	else line(frame, Point(x, y), Point(FRAME_WIDTH, y), colour, 2);

	putText(frame, objectName, Point(x, y + 30), 1, 1, colour, 2);// + intToString(x) + "," + intToString(y)
}

void drawObject(int x, int y, Mat &frame, std::string objectName, Scalar colour, int frame_height, int frame_width) {
    //use some of the openCV drawing functions to draw crosshairs
    //on your tracked image!

    circle(frame, Point(x, y), 20, colour, 2);

    if (y - 25>0)
        line(frame, Point(x, y), Point(x, y - 25), colour, 2);
    else line(frame, Point(x, y), Point(x, 0), colour, 2);
    if (y + 25<frame_height)
        line(frame, Point(x, y), Point(x, y + 25), colour, 2);
    else line(frame, Point(x, y), Point(x, frame_height), colour, 2);
    if (x - 25>0)
        line(frame, Point(x, y), Point(x - 25, y), colour, 2);
    else line(frame, Point(x, y), Point(0, y), colour, 2);
    if (x + 25<frame_width)
        line(frame, Point(x, y), Point(x + 25, y), colour, 2);
    else line(frame, Point(x, y), Point(frame_width, y), colour, 2);

    putText(frame, objectName + intToString(x) + "," + intToString(y), Point(x, y + 30), 1, 1, colour, 2);
}

ColourTrackerNode::ColourTrackerNode(
            int num_objects,
            int num_models,
            std::vector<std::string> object_names,
            std::vector<std::vector<int>> hsv_ranges,
            bool flags[6],
            int tracker_params[5],
            std::vector<int> crop_range,
            std::vector<double> resize_coeffs,
            int particle_filter_downsampling,
            std::vector<double> std_coeffs,
            sensor_msgs::CameraInfo rgb_cinfo,
            sensor_msgs::CameraInfo depth_cinfo,
            bool track_hand,
            std::vector<int> hand_hsv_range,
            std::vector<int> object_numbers) : 
                    node_handle_("~"),
                    num_models_(num_models),
                    object_names_(object_names),
                    crop_range_(crop_range),
                    resize_coeffs_(resize_coeffs),
                    particle_filter_downsampling_
                        (particle_filter_downsampling),
                    num_objects_(num_objects),
                    std_coeffs_(std_coeffs),
                    track_hand_(track_hand),
                    object_numbers_(object_numbers)
{
    std::cout << "Creating Colour Based Tracker Node\n";
    for (int i = 0; i < hsv_ranges.size(); i++) {
        ThingToFind box(hsv_ranges[i], object_names[i]);
        boxes_.push_back(box);
        Mat temp_colour(1, 1, CV_8UC3);
        temp_colour.at<Vec3b>(Point(0,0)) = Vec3b((hsv_ranges[i][0]+hsv_ranges[i][1])/2, (hsv_ranges[i][2]+hsv_ranges[i][3])/2, (hsv_ranges[i][4]+hsv_ranges[i][5])/2);
        cvtColor(temp_colour, temp_colour, COLOR_HSV2BGR);
        colours_.push_back(Scalar(temp_colour.at<Vec3b>(Point(0,0))[0],temp_colour.at<Vec3b>(Point(0,0))[1],temp_colour.at<Vec3b>(Point(0,0))[2]));
    }

    if (track_hand)
    {
        ThingToFind hand(hand_hsv_range, "Hand");
        hand_ = hand;
        Mat temp_colour(1, 1, CV_8UC3);
        temp_colour.at<Vec3b>(Point(0,0)) = Vec3b((hand_hsv_range[0]+hand_hsv_range[1])/2, (hand_hsv_range[2]+hand_hsv_range[3])/2, (hand_hsv_range[4]+hand_hsv_range[5])/2);
        cvtColor(temp_colour, temp_colour, COLOR_HSV2BGR);
        hand_colour_ = Scalar(temp_colour.at<Vec3b>(Point(0,0))[0],temp_colour.at<Vec3b>(Point(0,0))[1],temp_colour.at<Vec3b>(Point(0,0))[2]);
        //hand_scores_.push_back(0);
        //x_hand_.push_back(0);
        //y_hand_.push_back(0);
    }
    //    object_names.push_back("Hand");

    std::vector<int> temp;
    //store some zeros
    for (int j = 0; j < pastValues_; j++)
        temp.push_back(0);
    for (int i = 0; i < num_objects_; i++) {
        x_.push_back(0);
        y_.push_back(0);
        past_x_.push_back(temp);
        past_y_.push_back(temp);
        x_estimate_.push_back(0);
        y_estimate_.push_back(0);
        scores_.push_back(0);
    }
    show_RGB_ = flags[0];
    show_HSV_ = flags[1];
    show_threshold_ = flags[2];
    useTrackbars_ = flags[3];
    smooth_estimate_ = flags[4];
    hsv_ranges_available_ = !flags[5];
    MAX_NUM_OBJECTS_ = tracker_params[0];
    MIN_OBJECT_AREA_ = tracker_params[1];
    MAX_OBJECT_AREA_ = tracker_params[2];
    erode_size_ = tracker_params[3];
    dilate_size_ = tracker_params[4];

    //create slider bars for HSV filtering
    if (useTrackbars_) 
        createTrackbars();

    position_estimates_publisher_ = node_handle_.advertise<object_assembly_msgs::Points2D>("objects_Points2D", 0);
    input_publisher_ = node_handle_.advertise<object_assembly_msgs::Input>("input", 0);
    hsv_ranges_publisher_ = node_handle_.advertise<object_assembly_msgs::HSVRanges>("HSV_ranges", 0);

	if (show_RGB_)
        namedWindow(windowName, WINDOW_AUTOSIZE);
    if (!hsv_ranges_available_)
        selector_ = new Selector(windowName.c_str());

    //Colour to depth matching parameters
    x_focal_ratio_ = depth_cinfo.P[0] / rgb_cinfo.P[0];
    y_focal_ratio_ = depth_cinfo.P[5] / rgb_cinfo.P[5];
    x_offset_ = depth_cinfo.P[2] - (rgb_cinfo.P[2]-crop_range_[0]) * x_focal_ratio_;
    y_offset_ = depth_cinfo.P[6] - (rgb_cinfo.P[6]-crop_range_[1]) * y_focal_ratio_;
}

ColourTrackerNode::~ColourTrackerNode()
{
    delete crop_range_selector_;
    delete selector_;
}

void ColourTrackerNode::colour_tracker_callback(const sensor_msgs::ImageConstPtr& ros_image)
{ 
    Mat cameraFeed;
    try {
        (cv_bridge::toCvShare(ros_image,"bgr8")->image).copyTo(cameraFeed);
    }
    catch (cv_bridge::Exception& e) {
        ROS_ERROR("Could not convert from '%s' to 'bgr8'.", ros_image->encoding.c_str());
    }

    Rect r1(crop_range_[0], crop_range_[1], crop_range_[2], crop_range_[3]);
    cameraFeed = cameraFeed(r1);
    doStuff(cameraFeed);
}

void ColourTrackerNode::publish() {
    object_assembly_msgs::Points2D points, points_on_original;
    if (CLICKED_POINTS.size() < num_objects_) {
        for (int i = 0; i < x_.size(); i++) {
            object_assembly_msgs::Point2D point, point_on_original;
            point.x = x_estimate_[i];
            point.y = y_estimate_[i];
            point.name = "blah"; //TODO boxes_[i].name;
            points.points.push_back(point);
            if (scores_[i] < 0.001)
            {
                point_on_original.x = -1;
                point_on_original.y = -1;
            }
            else
            {
                point_on_original.x = x_estimate_[i] + crop_range_[0];
                point_on_original.y = y_estimate_[i] + crop_range_[1];
            }
            point_on_original.name = "blah";//boxes_[i].name;
            points_on_original.points.push_back(point_on_original);
        }
    }
    else {
        for (int i = 0; i < x_.size(); i++) {
            object_assembly_msgs::Point2D point, point_on_original;
            point.x = CLICKED_POINTS[i].x;
            point.y = CLICKED_POINTS[i].y;
            point.name = "blah";//boxes_[i].name;
            points.points.push_back(point);
            point_on_original.x = x_estimate_[i] + crop_range_[0];
            point_on_original.y = y_estimate_[i] + crop_range_[1];
            point_on_original.name = "blah";//boxes_[i].name;
            points_on_original.points.push_back(point_on_original);
        }
    }
    position_estimates_publisher_.publish(points_on_original);
    
    object_assembly_msgs::Input input;
    for (int i = 0; i < num_objects_; i++) {
        input.input.push_back((points.points[i].x*x_focal_ratio_+x_offset_)/particle_filter_downsampling_);
        input.input.push_back((points.points[i].y*y_focal_ratio_+y_offset_)/particle_filter_downsampling_);
        input.input.push_back(0.8); //TODO remove this line
    }
    for (int i = 0; i < num_objects_; i++) {
        input.input.push_back(scores_[i]);
    }
    if (track_hand_)
    {
        for (int i = 0; i < x_hand_.size(); i++)
        {
            input.input.push_back((x_hand_[i]*x_focal_ratio_+x_offset_)/particle_filter_downsampling_);
            input.input.push_back((y_hand_[i]*y_focal_ratio_+y_offset_)/particle_filter_downsampling_);
            input.input.push_back(0.8); //TODO remove this line
        }
        for (int i = 0; i < x_hand_.size(); i++)
        {
            input.input.push_back(hand_scores_[i]);
        }
    }
    input_publisher_.publish(input);
    object_assembly_msgs::HSVRanges ranges_msg;
    for (int i = 0; i < num_models_; i++)
        for (int j = 0; j < 6; j++)
            ranges_msg.ranges.push_back(boxes_[i].values[j]);
    hsv_ranges_publisher_.publish(ranges_msg);
}


void ColourTrackerNode::on_trackbar(int, void*) {
    //This function gets called whenever a
    // trackbar position is changed
}


void ColourTrackerNode::createTrackbars() {
    //create window for trackbars

    namedWindow(trackbarWindowName, 0);
    //create memory to store trackbar name on window

    //create trackbars and insert them into window
    //3 parameters are: the address of the variable that is changing when the trackbar is moved(eg.H_LOW),
    //the max value the trackbar can move (eg. H_HIGH), 
    //and the function that is called whenever the trackbar is moved(eg. on_trackbar)
    //                                  ---->    ---->     ---->      
    createTrackbar("H_MIN", trackbarWindowName, &H_MIN, H_MAX, on_trackbar);
    createTrackbar("H_MAX", trackbarWindowName, &H_MAX, H_MAX, on_trackbar);
    createTrackbar("S_MIN", trackbarWindowName, &S_MIN, S_MAX, on_trackbar);
    createTrackbar("S_MAX", trackbarWindowName, &S_MAX, S_MAX, on_trackbar);
    createTrackbar("V_MIN", trackbarWindowName, &V_MIN, V_MAX, on_trackbar);
    createTrackbar("V_MAX", trackbarWindowName, &V_MAX, V_MAX, on_trackbar);

}


void ColourTrackerNode::morphOps(Mat &thresh) {
    //create structuring element that will be used to "dilate" and "erode" image.
    //the element chosen here is a 3px by 3px rectangle

    Mat erodeElement = getStructuringElement(MORPH_RECT, Size(erode_size_, erode_size_));
    //dilate with larger element so make sure object is nicely visible
    Mat dilateElement = getStructuringElement(MORPH_RECT, Size(dilate_size_, dilate_size_));

    erode(thresh, thresh, erodeElement);
    erode(thresh, thresh, erodeElement);

    dilate(thresh, thresh, dilateElement);
    dilate(thresh, thresh, dilateElement);
}


double ColourTrackerNode::smoothEstimate(std::vector<int> &past_values,
                          int current_value) {
//TODO smooth based only on previous estimate
    double estimate = 0.0312*current_value; //extra to add up to 1
    past_values.erase(past_values.begin(), past_values.begin()+1);
    past_values.push_back(current_value);
    for (int k=past_values.size()-1;k>=0;k--) {
        estimate = estimate + past_values[k]/pow(2,k+1);
    }
    return estimate;
}


bool ColourTrackerNode::trackFilteredObject(
            int &x,
            int &y,
            Mat threshold,
            Mat &cameraFeed,
            std::string objectName,
            double &score)
{
    Mat temp;
    threshold.copyTo(temp);
    //these two vectors needed for output of findContours
    std::vector< std::vector<Point> > contours;
    std::vector<Vec4i> hierarchy;
    //find contours of filtered image using openCV findContours function
    findContours(temp, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
    //use moments method to find our filtered object
    double refArea = 0;
    double totalArea = 0;
    bool objectFound = false;
    if (hierarchy.size() > 0)
    {
        int numObjects = hierarchy.size();
        //if number of objects greater than MAX_NUM_OBJECTS_ we have a noisy filter
        if (numObjects<MAX_NUM_OBJECTS_)
        {
            for (int index = 0; index >= 0; index = hierarchy[index][0])
            {
                Moments moment = moments((cv::Mat)contours[index]);
                double area = moment.m00;
                totalArea += area;                

                if (area>MIN_OBJECT_AREA_ && area<MAX_OBJECT_AREA_ && area>refArea)
                {
                    x = moment.m10 / area;
                    y = moment.m01 / area;
                    objectFound = true;
                    refArea = area;
                }
            }
        }
        else putText(cameraFeed, "TOO MUCH NOISE! ADJUST FILTER", Point(0, 50), 1, 2, Scalar(0, 0, 255), 2);
    }
    if (totalArea != 0)
        score = refArea/totalArea;
    else
        score = 0;
    return objectFound;
}

bool ColourTrackerNode::trackMultipleFilteredObjects(
            std::vector<int> &x,
            std::vector<int> &y,
            std::vector<int> &prev_x,
            std::vector<int> &prev_y,
            Mat threshold,
            Mat &cameraFeed,
            std::vector<double> &scores)
{
    Mat temp;
    threshold.copyTo(temp);
    //these two vectors needed for output of findContours
    std::vector< std::vector<Point> > contours;
    std::vector<Vec4i> hierarchy;
    //find contours of filtered image using openCV findContours function
    findContours(temp, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
    //use moments method to find our filtered object
    double refArea = 0;
    double totalArea = 0;
    bool objectFound = false;
    std::vector<double> areas;
    std::vector<int> contours_x;
    std::vector<int> contours_y;
    double smallest_to_keep = std::numeric_limits<double>::infinity();
    if (hierarchy.size() > 0 && hierarchy.size() < MAX_NUM_OBJECTS_)
    {
        //if number of objects greater than MAX_NUM_OBJECTS_ we have a noisy filter
        for (int index = 0; index >= 0; index = hierarchy[index][0])
        {
            Moments moment = moments((cv::Mat)contours[index]);
            double area = moment.m00;
            totalArea += area;
            areas.push_back(area);
            contours_x.push_back(moment.m10 / area);
            contours_y.push_back(moment.m01 / area);
        }
        std::vector<long unsigned int> area_ind_sorted = sort_indexes(areas);
        std::vector<std::vector<int>> M;
        M.resize(prev_x.size(), std::vector<int>(std::min(area_ind_sorted.size(), prev_x.size()), 0));
        for (int obj = 0; obj < prev_x.size(); obj++)
            for (int i = 0; i < std::min(area_ind_sorted.size(), prev_x.size()); i++)
                M[obj][i] = std::sqrt(std::pow(contours_x[area_ind_sorted[i]]-prev_x[obj], 2) + std::pow(contours_y[area_ind_sorted[i]]-prev_y[obj], 2));

        Hungarian hungarian(M , prev_x.size(), std::min(area_ind_sorted.size(), prev_x.size()), HUNGARIAN_MODE_MINIMIZE_COST);
        //hungarian.print_cost();
        hungarian.solve();
        std::vector<std::vector<int>> hungarian_ass = hungarian.assignment();
        //hungarian.print_assignment();
        for (int obj = 0; obj < prev_x.size(); obj++)
            for (int i = 0; i < std::min(area_ind_sorted.size(), prev_x.size()); i++)
                if (hungarian_ass[obj][i] == 1)
                {
                    x[obj] = contours_x[area_ind_sorted[i]];
                    y[obj] = contours_y[area_ind_sorted[i]];
                    scores[obj] = 1; //areas[area_ind_sorted[i]]/totalArea); //TODO fix
                    break;
                }
        objectFound = true;
    }
    else for (int i = 0; i < scores.size(); i++) scores[i] = 0;
    return objectFound;
}


bool ColourTrackerNode::trackHand(
            std::vector<int> &x,
            std::vector<int> &y,
            Mat threshold,
            std::string objectName,
            std::vector<double> &scores)
{
    std::vector<int> new_x;
    std::vector<int> new_y;
    std::vector<double> new_scores;
    x = new_x;
    y = new_y;
    scores = new_scores;
    Mat temp;
    threshold.copyTo(temp);
    //these two vectors needed for output of findContours
    std::vector< std::vector<Point> > contours;
    std::vector<Vec4i> hierarchy;
    //find contours of filtered image using openCV findContours function
    findContours(temp, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
    //use moments method to find our filtered object
    double refArea = 0;
    double totalArea = 0;
    bool objectFound = false;
    int num_points = 0;
    if (hierarchy.size() > 0)
    {
        for (int index = 0; index >= 0; index = hierarchy[index][0])
        {
            Moments moment = moments((cv::Mat)contours[index]);
            double area = moment.m00;
            totalArea += area;
        }
        
        for (int index = 0; index >= 0; index = hierarchy[index][0])
        {
            Moments moment = moments((cv::Mat)contours[index]);
            double area = moment.m00;   
            if (area > totalArea/5 && area>MIN_OBJECT_AREA_ && area<MAX_OBJECT_AREA_)
            {
                num_points++;
                x.push_back(moment.m10 / area);
                y.push_back(moment.m01 / area);
                objectFound = true;
                scores.push_back(1);
            }
        }
    }
    return objectFound;
}


void ColourTrackerNode::doStuff(Mat &cameraFeed) {
    //convert frame from BGR to HSV colorspace
    cvtColor(cameraFeed, HSV_, COLOR_BGR2HSV);
    if (hsv_ranges_available_)
    {
        std::string objectName;
        int obj = 0;
        //repeat for all models
        for (int j = 0; j < boxes_.size(); j++)
        {
            for (int i = 0; i < 6; i++)
		            minmax_[i] = boxes_[j].values[i];
		        objectName = boxes_[j].name;
            //filter HSV image between values and store filtered 
            //image to threshold matrix
            if (useTrackbars_)
                inRange(HSV_, Scalar(H_MIN, S_MIN, V_MIN), Scalar(H_MAX, S_MAX, V_MAX), threshold_);
            else
                if (minmax_[0] >= 0)
                    inRange(HSV_, Scalar(minmax_[0], minmax_[2], minmax_[4]), Scalar(minmax_[1], minmax_[3], minmax_[5]), threshold_);
                else
                {
                    Mat tmp1, tmp2;
                    inRange(HSV_, Scalar(0, minmax_[2], minmax_[4]), Scalar(minmax_[1], minmax_[3], minmax_[5]), tmp1);
                    inRange(HSV_, Scalar(180+minmax_[0], minmax_[2], minmax_[4]), Scalar(180, minmax_[3], minmax_[5]), tmp2);
                    bitwise_or(tmp1, tmp2, threshold_);
                }
                
            if (useMorphOps_)
                morphOps(threshold_);

            if (object_numbers_[j] == 1)
            {
                if (trackFilteredObject(x_[obj], y_[obj], threshold_, cameraFeed, objectName, scores_[obj]))
                {
                    if (smooth_estimate_)
                    {
                        x_estimate_[obj] = smoothEstimate(past_x_[obj], x_[obj]);
                        y_estimate_[obj] = smoothEstimate(past_y_[obj], y_[obj]);
                    }
                    else
                    {
                        x_estimate_[obj] = (double)x_[obj];
                        y_estimate_[obj] = (double)y_[obj];
                    }
                    drawObject((int)x_estimate_[obj], (int)y_estimate_[obj], cameraFeed, objectName, colours_[j]);
                }
            }
            else
            {
                std::vector<int> x_v, y_v, prev_x, prev_y;
                std::vector<double> scores;
                for (int i = obj; i < obj + object_numbers_[j]; i++)
                {
                    x_v.push_back(x_[i]);
                    y_v.push_back(y_[i]);
                    prev_x.push_back(x_estimate_[i]);
                    prev_y.push_back(y_estimate_[i]);
                    scores.push_back(scores_[i]);
                }
                if (trackMultipleFilteredObjects(x_v, y_v, prev_x, prev_y, threshold_, cameraFeed, scores))
                    for (int i = obj; i < obj + object_numbers_[j]; i++) 
                    {
                        if (smooth_estimate_)
                        {
                            x_estimate_[i] = smoothEstimate(past_x_[i], x_v[i-obj]);
                            y_estimate_[i] = smoothEstimate(past_y_[i], y_v[i-obj]);
                        }
                        else
                        {
                            x_estimate_[i] = (double)x_v[i-obj];
                            y_estimate_[i] = (double)y_v[i-obj];
                        }
                        drawObject((int)x_estimate_[i], (int)y_estimate_[i], cameraFeed, objectName+std::to_string(i-obj+1), colours_[j]);
                    }
                for (int i = obj; i < obj + object_numbers_[j]; i++)
                {
                    x_[i] = x_v[i-obj];
                    y_[i] = y_v[i-obj];
                    scores_[i] = scores[i-obj];
                }
            }
            obj += object_numbers_[j];
        }
        if (track_hand_)
        {
            for (int i = 0; i < 6; i++)
		            minmax_[i] = hand_.values[i];
		        objectName = hand_.name;
            //filter HSV image between values and store filtered 
            //image to threshold matrix
            if (useTrackbars_)
                inRange(HSV_, Scalar(H_MIN, S_MIN, V_MIN), Scalar(H_MAX, S_MAX, V_MAX), threshold_);
            else
                inRange(HSV_, Scalar(minmax_[0], minmax_[2], minmax_[4]), Scalar(minmax_[1], minmax_[3], minmax_[5]), threshold_);
                
            if (useMorphOps_)
                morphOps(threshold_);

            if (trackHand(x_hand_, y_hand_, threshold_, objectName, hand_scores_))
            {
            //let user know an object was found
                //if (smooth_estimate_)
                //{
                //    x_estimate_hand_ = smoothEstimate(past_x_hand_, x_hand_);
                //    y_estimate_hand_ = smoothEstimate(past_y_hand_, y_hand_);
                //}
                //else
                //{
                //    x_estimate_hand_ = (double)x_hand_;
                //    y_estimate_hand_ = (double)y_hand_;
                //}
                for (int i = 0; i < x_hand_.size(); i++)
                    drawObject(x_hand_[i], y_hand_[i], cameraFeed, objectName, hand_colour_);
            }
        }

        publish();


    }
    else //Use selector to select colors
    {
        Mat rgb[3];
        split(cameraFeed, rgb);
        Mat mask = rgb[0] != 0;
        bitwise_or(rgb[1]!=0, mask, mask);
        bitwise_or(rgb[2]!=0, mask, mask);
        if (selector_->calculate_ranges(HSV_, means_, stds_, mask) >= num_models_ + (int)track_hand_)
        {
            hsv_ranges_available_ = true;
            for (int obj = 0; obj < num_models_; obj++)
            {
                ThingToFind box(std::vector<int>(6,0), object_names_[obj]);
                box.values[0] = (int)(means_[obj].at<double>(0,0) - std_coeffs_[0] * stds_[obj].at<double>(0,0));
                box.values[2] = (int)(means_[obj].at<double>(1,0) - std_coeffs_[1] * stds_[obj].at<double>(1,0));
                box.values[4] = (int)(means_[obj].at<double>(2,0) - std_coeffs_[2] * stds_[obj].at<double>(2,0));
                box.values[1] = (int)(means_[obj].at<double>(0,0) + std_coeffs_[0] * stds_[obj].at<double>(0,0));
                box.values[3] = (int)(means_[obj].at<double>(1,0) + std_coeffs_[1] * stds_[obj].at<double>(1,0));
                box.values[5] = (int)(means_[obj].at<double>(2,0) + std_coeffs_[2] * stds_[obj].at<double>(2,0));
                if (box.values[1] > 180)
                {
                    box.values[0] -= 180;
                    box.values[1] -= 180;
                }
                boxes_.push_back(box);
                Mat temp_colour(1, 1, CV_8UC3);
                temp_colour.at<Vec3b>(Point(0,0)) = Vec3b((box.values[0]+box.values[1])/2, (box.values[2]+box.values[3])/2, (box.values[4]+box.values[5])/2);
                cvtColor(temp_colour, temp_colour, COLOR_HSV2BGR);
                colours_.push_back(Scalar(temp_colour.at<Vec3b>(Point(0,0))[0],temp_colour.at<Vec3b>(Point(0,0))[1],temp_colour.at<Vec3b>(Point(0,0))[2]));
            }
            if (track_hand_)
            {
                hand_.values[0] = (int)(means_[num_models_].at<double>(0,0) - std_coeffs_[0] * stds_[num_models_].at<double>(0,0));
                hand_.values[2] = (int)(means_[num_models_].at<double>(1,0) - std_coeffs_[1] * stds_[num_models_].at<double>(1,0));
                hand_.values[4] = (int)(means_[num_models_].at<double>(2,0) - std_coeffs_[2] * stds_[num_models_].at<double>(2,0));
                hand_.values[1] = (int)(means_[num_models_].at<double>(0,0) + std_coeffs_[0] * stds_[num_models_].at<double>(0,0));
                hand_.values[3] = (int)(means_[num_models_].at<double>(1,0) + std_coeffs_[1] * stds_[num_models_].at<double>(1,0));
                hand_.values[5] = (int)(means_[num_models_].at<double>(2,0) + std_coeffs_[2] * stds_[num_models_].at<double>(2,0));
                if (hand_.values[1] > 180)
                {
                    hand_.values[0] -= 180;
                    hand_.values[1] -= 180;
                }
                Mat temp_colour(1, 1, CV_8UC3);
                temp_colour.at<Vec3b>(Point(0,0)) = Vec3b((minmax_[0]+minmax_[1])/2, (minmax_[2]+minmax_[3])/2, (minmax_[4]+minmax_[5])/2);
                cvtColor(temp_colour, temp_colour, COLOR_HSV2BGR);
                hand_colour_ = Scalar(temp_colour.at<Vec3b>(Point(0,0))[0],
                                      temp_colour.at<Vec3b>(Point(0,0))[1],
                                      temp_colour.at<Vec3b>(Point(0,0))[2]);
            }
        }
    }

    if (show_threshold_)
        imshow(windowName2, threshold_);
    if (show_RGB_)
    {
        if (!hsv_ranges_available_)
            if (selector_->m_selecting)
                rectangle(cameraFeed, selector_->m_selection, Scalar(0,127,0));
        imshow(windowName, cameraFeed);
    }
    if (show_HSV_)
        imshow(windowName1, HSV_);

    waitKey(30);
}


int main(int argc, char* argv[]) {
    ros::init(argc, argv, "colour_based_tracker");
    ros::NodeHandle nh("~");

    //Read params from config file
    int num_models;
    nh.getParam("number_of_models", num_models);

    std::string camera_topic;
    nh.getParam("camera_topic", camera_topic);
    bool flags[6];
    nh.getParam("show_rgb", flags[0]);
    nh.getParam("show_hsv", flags[1]);
    nh.getParam("show_threshold", flags[2]);
    nh.getParam("use_trackbars", flags[3]);
    nh.getParam("smooth_estimate", flags[4]);
    nh.getParam("interactive_hsv_ranges", flags[5]);

    std::vector<std::string> object_names;
    std::vector<std::vector<int>> hsv_ranges;
    std::vector<int> object_numbers;
    int num_objects;
    for (int i = 0; i < num_models; i++) {
        std::string object_pre = std::string("models/") + "model" + std::to_string(i+1) + "/";
        std::string object_name;
        nh.getParam(object_pre + "name", object_name);
        object_names.push_back(object_name);
        int num;
        nh.param(object_pre + "number", num, 1);
        object_numbers.push_back(num);
        num_objects += num;
        if (!flags[5])
        {
            std::vector<int> hsv_range;
            nh.getParam(object_pre + "hsv_range", hsv_range);
            hsv_ranges.push_back(hsv_range); 
        }       
    }
    std::vector<int> hand_hsv_range;
    nh.getParam("hand_range", hand_hsv_range);
    bool track_hand;
    nh.getParam("track_hand", track_hand);

    int tracker_params[5];
    nh.getParam("MAX_NUM_OBJECTS", tracker_params[0]);
    nh.getParam("MIN_OBJECT_AREA", tracker_params[1]);
    nh.getParam("MAX_OBJECT_AREA", tracker_params[2]);
    nh.getParam("erode_size", tracker_params[3]);
    nh.getParam("dilate_size", tracker_params[4]);

    bool get_crop_range_from_topic;
    std::vector<double> resize_coeffs;
    nh.getParam("resize_coefficients", resize_coeffs);
    if (resize_coeffs.size() != 2)
    {
        ROS_ERROR("resize_coefficients must have two values");
        return -1;
    }
    nh.getParam("get_crop_range_from_topic", get_crop_range_from_topic);
    std::vector<int> crop_range;
    if (get_crop_range_from_topic)
    {
        std::string crop_range_topic;
        nh.getParam("crop_range_topic", crop_range_topic);
        ROS_INFO("Colour tracker: Waiting for crop range...\n");
        std_msgs::Int32MultiArray crop_msg = *ros::topic::waitForMessage<std_msgs::Int32MultiArray>(crop_range_topic);
        crop_range = crop_msg.data;
        crop_range[0] /= resize_coeffs[0];
        crop_range[1] /= resize_coeffs[1];
        crop_range[2] /= resize_coeffs[0];
        crop_range[3] /= resize_coeffs[1];
    }
    else
        nh.getParam("crop_range", crop_range);
    std::cout << "Colour crop range: " << crop_range[0] << " " << crop_range[1] << " " << crop_range[2] << " " << crop_range[3] << "\n";
    int particle_filter_downsampling;
    nh.getParam("particle_filter_downsampling", particle_filter_downsampling);
    std::vector<double> std_coeffs;
    nh.getParam("std_coefficients", std_coeffs);

    std::string rgb_camera_info_topic, depth_camera_info_topic;
    nh.getParam("rgb_camera_info_topic", rgb_camera_info_topic);
    nh.getParam("depth_camera_info_topic", depth_camera_info_topic);
    std::cout << "Colour tracker waiting for topic: " << rgb_camera_info_topic << "\n";
    sensor_msgs::CameraInfo rgb_cinfo = *ros::topic::waitForMessage<sensor_msgs::CameraInfo>(rgb_camera_info_topic);
    std::cout << "Colour tracker waiting for topic: " << depth_camera_info_topic << "\n";
    sensor_msgs::CameraInfo depth_cinfo = *ros::topic::waitForMessage<sensor_msgs::CameraInfo>(depth_camera_info_topic);
    std::cout << "Colour tracker: Got topics\n";

    ColourTrackerNode colour_tracker_node(num_objects, num_models, object_names, hsv_ranges, flags, tracker_params, crop_range, resize_coeffs, particle_filter_downsampling, std_coeffs, rgb_cinfo, depth_cinfo, track_hand, hand_hsv_range, object_numbers);

    ros::Subscriber subscriber = nh.subscribe(camera_topic, 1, &ColourTrackerNode::colour_tracker_callback, &colour_tracker_node);

    ros::spin();
}
