#include <dbot/connection_info.hpp>

ConnectionInfoList::ConnectionInfoList(int num_objects, std::vector<ConnectionInfo> connection_list) : num_objects_(num_objects), connections_(connection_list)
{
    std::vector<std::vector<int>> connected_verts(num_objects_);
    std::vector<std::vector<int>> connected_edges(num_objects_);
     // Find immediate neighbours
    for (int i = 0; i < connections_.size(); i++)
    {
        for (int i_vert = 0; i_vert < num_objects_; i_vert++)
        {
            if (connections_[i].part == i_vert)
            {
                connected_verts[i_vert].push_back(connections_[i].relative_part);
                connected_edges[i_vert].push_back(i);
            }
            if (connections_[i].relative_part == i_vert)
            {
                connected_verts[i_vert].push_back(connections_[i].part);
                connected_edges[i_vert].push_back(i);
            }
        }
    }

    // Construct connection_indices
    for (int i = 0; i < num_objects_; i++)
    {
        std::vector<int> indices;
        std::vector<int> visited(num_objects_, 0);
        visited[i] = 1;
        check_vertex(visited, indices, connected_verts, connected_edges, i);
        connection_indices.push_back(indices);
    }
}


void ConnectionInfoList::check_vertex(std::vector<int> &visited,
      std::vector<int> &indices,
      std::vector<std::vector<int>> connected_verts,
      std::vector<std::vector<int>> connected_edges,
      int vertex)
{
    for (int i_vert = 0; i_vert < connected_verts[vertex].size(); i_vert++)
    {
        if (!visited[connected_verts[vertex][i_vert]])
        {
            visited[connected_verts[vertex][i_vert]] = 1;
            indices.push_back(connected_edges[vertex][i_vert]);
            check_vertex(visited, indices, connected_verts, connected_edges, connected_verts[vertex][i_vert]);
        }
    }
}


ConnectionInfoList::ConnectionInfo ConnectionInfoList::connection_info(int ind)
{
    return connections_[ind];
}

