#ifndef CONNECTION_INFO_HPP
#define CONNECTION_INFO_HPP

#include <vector>
#include <iostream>
#include <Eigen/Core>
#include <Eigen/Geometry> 

//source: http://stackoverflow.com/questions/26763193/return-a-list-of-connected-component-subgraphs-in-boost-graph


class ConnectionInfoList
{
public:
    struct ConnectionInfo
    {
        int part; //zero based
        int relative_part;
        Eigen::Transform<double, 3, Eigen::Affine> relative_pose;
        int num_particles;
    };

    ConnectionInfoList(int num_objects, std::vector<ConnectionInfo> connection_list);

    ConnectionInfo connection_info(int ind);

private:
    void check_vertex(std::vector<int> &visited,
          std::vector<int> &indices,
          std::vector<std::vector<int>> connected_verts,
          std::vector<std::vector<int>> connected_edges,
          int vertex);

public:
    // For each part, a list of connection indices
    std::vector<std::vector<int>> connection_indices;

public:
    std::vector<ConnectionInfo> connections_;
    const int num_objects_;
};

#endif
