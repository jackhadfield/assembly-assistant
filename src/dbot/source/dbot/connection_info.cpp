#include <dbot/connection_info.hpp>

ConnectionInfoList::ConnectionInfoList(int num_objects, std::vector<ConnectionInfo> connection_list) : num_objects_(num_objects), connections_(connection_list)
{
}

ConnectionInfoList::ConnectionInfo ConnectionInfoList::connection_info(int ind)
{
    return connections_[ind];
}

