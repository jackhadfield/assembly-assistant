#ifndef CONNECTION_INFO_HPP
#define CONNECTION_INFO_HPP

#include <vector>
#include <iostream>
#include <Eigen/Core>
#include <Eigen/Geometry> 

class ConnectionInfoList
{
public:
    struct ConnectionInfo
    {
        int part; //zero based
        int relative_part;
        Eigen::Transform<double, 3, Eigen::Affine> relative_pose;
        double num_particles;
        double first_particle;
    };

    ConnectionInfoList(int num_objects, std::vector<ConnectionInfo> connection_list);

    ConnectionInfo connection_info(int ind);

public:
    // For each part, a list of connection indices
    std::vector<std::vector<int>> connection_indices;

public:
    std::vector<ConnectionInfo> connections_;
    const int num_objects_;
};

#endif
