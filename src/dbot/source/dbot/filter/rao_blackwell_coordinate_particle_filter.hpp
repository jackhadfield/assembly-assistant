/*
 * This is part of the Bayesian Object Tracking (bot),
 * (https://github.com/bayesian-object-tracking)
 *
 * Copyright (c) 2015 Max Planck Society,
 * 				 Autonomous Motion Department,
 * 			     Institute for Intelligent Systems
 *
 * This Source Code Form is subject to the terms of the GNU General Public
 * License License (GNU GPL). A copy of the license can be found in the LICENSE
 * file distributed with this source code.
 */

/*
 * This file implements a part of the algorithm published in:
 *
 * M. Wuthrich, J. Bohg, D. Kappler, C. Pfreundt, S. Schaal
 * The Coordinate Particle Filter -
 * A novel Particle Filter for High Dimensional Systems
 * IEEE Intl Conf on Robotics and Automation, 2015
 * http://arxiv.org/abs/1505.00251
 *
 */

#pragma once

#include <vector>
#include <limits>
#include <string>
#include <memory>
#include <functional>

#include <Eigen/Core>

#include <fl/util/types.hpp>
#include <fl/distribution/gaussian.hpp>
#include <fl/distribution/discrete_distribution.hpp>
#include <fl/util/profiling.hpp>

#include <dbot/traits.hpp>
#include <dbot/model/rao_blackwell_sensor.hpp>
#include <dbot/connection_info.hpp>
#include <osr/free_floating_rigid_bodies_state.hpp>

namespace dbot
{
template <typename Transition, typename Sensor>
class RaoBlackwellCoordinateParticleFilter
{
public:
    typedef typename Transition::State State;
    typedef typename Transition::Input Input;
    typedef typename Transition::Noise Noise;

    typedef Eigen::Array<State, -1, 1> StateArray;
    typedef Eigen::Array<fl::Real, -1, 1> RealArray;
    typedef Eigen::Array<int, -1, 1> IntArray;

    typedef typename Sensor::Observation Observation;

    typedef fl::DiscreteDistribution<State> Belief;

public:
    /// constructor and destructor *********************************************
    RaoBlackwellCoordinateParticleFilter(
        const std::shared_ptr<Transition> transition,
        const std::shared_ptr<Sensor> sensor,
        const std::vector<std::vector<int>>& sampling_blocks,
        const fl::Real& max_kl_divergence = 0)
        : sensor_(sensor),
          transition_(transition),
          max_kl_divergence_(max_kl_divergence)
    {
        sampling_blocks_ = sampling_blocks;

        // make sure sizes are consistent --------------------------------------
        size_t dimension = 0;
        for (size_t i = 0; i < sampling_blocks_.size(); i++)
        {
            dimension += sampling_blocks_[i].size();
        }
        if (dimension != transition_->noise_dimension())
        {
            std::cout << "the dimension of the sampling blocks is " << dimension
                      << " while the dimension of the noise is "
                      << transition_->noise_dimension() << std::endl;
            exit(-1);
        }
    }
    virtual ~RaoBlackwellCoordinateParticleFilter() noexcept {}
    /// the filter functions ***************************************************
    void filter(const Observation& observation, const Input& input)
    {
        sensor_->set_observation(observation);

        loglikes_ = RealArray::Zero(belief_.size());
        noises_ = std::vector<Noise>(
            belief_.size(), Noise::Zero(transition_->noise_dimension()));
        old_particles_ = belief_.locations();
        for (size_t i_block = 0; i_block < sampling_blocks_.size(); i_block++)
        {
            // add noise of this block -----------------------------------------
            for (size_t i_sampl = 0; i_sampl < belief_.size(); i_sampl++)
            {
                for (size_t i = 0; i < sampling_blocks_[i_block].size(); i++)
                {
                    noises_[i_sampl](sampling_blocks_[i_block][i]) =
                        unit_gaussian_.sample()(0);
                }
            }

            // propagate using partial noise -----------------------------------
            for (size_t i_sampl = 0; i_sampl < belief_.size(); i_sampl++)
            {
                belief_.location(i_sampl) = transition_->state(
                    old_particles_[i_sampl], noises_[i_sampl], input);
            }

            // compute likelihood ----------------------------------------------
            bool update = (i_block == sampling_blocks_.size() - 1);
            RealArray new_loglikes = sensor_->loglikes(
                belief_.locations(), indices_, update);

            // update the weights and resample if necessary --------------------
            belief_.delta_log_prob_mass(new_loglikes - loglikes_);
            loglikes_ = new_loglikes;

            if (belief_.kl_given_uniform() > max_kl_divergence_)
            {
                resample(belief_.size());
            }
        }
    }

    void filter(
        const Observation& observation,
        const Input& input,
        const ConnectionInfoList& connection_list)
    {
        sensor_->set_observation(observation);

        loglikes_ = RealArray::Zero(belief_.size());
        noises_ = std::vector<Noise>(
            belief_.size(), Noise::Zero(transition_->noise_dimension()));
        old_particles_ = belief_.locations();

        check_intersections_.clear();
        for (size_t i = 0; i < sensor_->num_objects(); i++)
        {
            std::vector<bool> temp_bools;
            for (size_t j = 0; j < i; j++)
                temp_bools.push_back(true);
            check_intersections_.push_back(temp_bools);
        }

        for (size_t i_block = 0; i_block < sampling_blocks_.size(); i_block++)
        {
            // add noise of this block -----------------------------------------
            for (size_t i_sampl = 0; i_sampl < belief_.size(); i_sampl++)
            {
                for (size_t i = 0; i < sampling_blocks_[i_block].size(); i++)
                {
                    noises_[i_sampl](sampling_blocks_[i_block][i]) =
                        unit_gaussian_.sample()(0);
                }
            }

            // propagate using partial noise -----------------------------------
            for (size_t i_sampl = 0; i_sampl < belief_.size(); i_sampl++)
            {
                belief_.location(i_sampl) = transition_->state(
                    old_particles_[i_sampl], noises_[i_sampl], input);
            }
            for (int i = 0; i < connection_list.connections_.size(); i++)
            {
                auto pose_0 = sensor_->integrated_poses().component(connection_list.connections_[i].part);
                auto pose_1 = sensor_->integrated_poses().component(connection_list.connections_[i].relative_part);
                //TODO: make this readable!
                for (size_t i_sampl = connection_list.connections_[i].first_particle; i_sampl < connection_list.connections_[i].first_particle + connection_list.connections_[i].num_particles; i_sampl+=2)
                {
                    belief_.location(i_sampl).component(connection_list.connections_[i].part).position() = pose_0.orientation().rotation_matrix().transpose() * (pose_1.orientation().rotation_matrix() * (belief_.location(i_sampl).component(connection_list.connections_[i].relative_part).position() + belief_.location(i_sampl).component(connection_list.connections_[i].relative_part).orientation().rotation_matrix() * connection_list.connections_[i].relative_pose.translation()) + pose_1.position() - pose_0.position());

                    osr::FreeFloatingRigidBodiesState<> tempPoseArray;
                    tempPoseArray.recount(1);
                    tempPoseArray.setZero();
                    tempPoseArray.component(0).orientation().rotation_matrix(connection_list.connections_[i].relative_pose.rotation());
                    belief_.location(i_sampl).component(connection_list.connections_[i].part).orientation() = pose_0.orientation().inverse() * pose_1.orientation() * belief_.location(i_sampl).component(connection_list.connections_[i].relative_part).orientation() * tempPoseArray.component(0).orientation();

                    if (connection_list.connections_[i].part > connection_list.connections_[i].relative_part)
                        check_intersections_[connection_list.connections_[i].part][connection_list.connections_[i].relative_part] = false;
                    else
                        check_intersections_[connection_list.connections_[i].relative_part][connection_list.connections_[i].part] = false;
                }
            }

            for (int i = connection_list.connections_.size()-1; i >= 0; i--)
            {
                auto pose_0 = sensor_->integrated_poses().component(connection_list.connections_[i].part);
                auto pose_1 = sensor_->integrated_poses().component(connection_list.connections_[i].relative_part);
                for (size_t i_sampl = connection_list.connections_[i].first_particle+1; i_sampl < connection_list.connections_[i].first_particle + connection_list.connections_[i].num_particles; i_sampl+=2)
                {
                    osr::FreeFloatingRigidBodiesState<> tempPoseArray;
                    tempPoseArray.recount(1);
                    tempPoseArray.setZero();
                    tempPoseArray.component(0).orientation().rotation_matrix(connection_list.connections_[i].relative_pose.rotation());
                    belief_.location(i_sampl).component(connection_list.connections_[i].relative_part).orientation() = pose_1.orientation().inverse() * pose_0.orientation() * belief_.location(i_sampl).component(connection_list.connections_[i].part).orientation() * tempPoseArray.component(0).orientation().inverse();

                    belief_.location(i_sampl).component(connection_list.connections_[i].relative_part).position() = pose_1.orientation().rotation_matrix().transpose() * (pose_0.orientation().rotation_matrix() * (belief_.location(i_sampl).component(connection_list.connections_[i].part).position() + belief_.location(i_sampl).component(connection_list.connections_[i].part).orientation().rotation_matrix() * (-(tempPoseArray.component(0).orientation().rotation_matrix().transpose()*connection_list.connections_[i].relative_pose.translation()))) + pose_0.position() - pose_1.position());

                    if (connection_list.connections_[i].part > connection_list.connections_[i].relative_part)
                        check_intersections_[connection_list.connections_[i].part][connection_list.connections_[i].relative_part] = false;
                    else
                        check_intersections_[connection_list.connections_[i].relative_part][connection_list.connections_[i].part] = false;
                }
            }


            // compute likelihood ----------------------------------------------
            bool update = (i_block == sampling_blocks_.size() - 1);
            RealArray new_loglikes = sensor_->loglikes(
                belief_.locations(), indices_, check_intersections_, update);

            // update the weights and resample if necessary --------------------
            belief_.delta_log_prob_mass(new_loglikes - loglikes_);
            loglikes_ = new_loglikes;
            if (belief_.kl_given_uniform() > max_kl_divergence_)
            {
                resample(belief_.size());
            }
        }
    }

    void filter(const Observation& observation, const Input& input, const ConnectionInfoList& connection_list, const Eigen::Matrix<double, -1, 1> input_scores)
    {
        sensor_->set_observation(observation);

        loglikes_ = RealArray::Zero(belief_.size());
        noises_ = std::vector<Noise>(
            belief_.size(), Noise::Zero(transition_->noise_dimension()));
        old_particles_ = belief_.locations();

        check_intersections_.clear();
        for (size_t i = 0; i < sensor_->num_objects(); i++)
        {
            std::vector<bool> temp_bools;
            for (size_t j = 0; j < i; j++)
                temp_bools.push_back(true);
            check_intersections_.push_back(temp_bools);
        }

        for (size_t i_block = 0; i_block < sampling_blocks_.size(); i_block++)
        {
            // add noise of this block -----------------------------------------
            for (size_t i_sampl = 0; i_sampl < belief_.size(); i_sampl++)
            {
                for (size_t i = 0; i < sampling_blocks_[i_block].size(); i++)
                {
                    noises_[i_sampl](sampling_blocks_[i_block][i]) =
                        unit_gaussian_.sample()(0);
                }
            }

            // propagate using partial noise -----------------------------------
            for (size_t i_sampl = 0; i_sampl < belief_.size(); i_sampl++)
            {
                Input particle_input;
                particle_input.resize(3*sensor_->num_objects(),1);
                for (size_t i_part = 0; i_part < sensor_->num_objects(); i_part++)
                {
                    particle_input.block(i_part*3,0,3,1) = (sensor_->integrated_poses().component(i_part).orientation().rotation_matrix().transpose() * (input.block(i_part*3,0,3,1) - sensor_->integrated_poses().component(i_part).position()) - old_particles_[i_sampl].component(i_part).position()).cwiseProduct(input_scores.block(i_part*3,0,3,1));
                }

                belief_.location(i_sampl) = transition_->state(
                    old_particles_[i_sampl], noises_[i_sampl], particle_input);
            }
            for (int i = 0; i < connection_list.connections_.size(); i++)
            {
                auto pose_0 = sensor_->integrated_poses().component(connection_list.connections_[i].part);
                auto pose_1 = sensor_->integrated_poses().component(connection_list.connections_[i].relative_part);
                //TODO: make this readable!
                for (size_t i_sampl = connection_list.connections_[i].first_particle * belief_.size(); i_sampl < connection_list.connections_[i].first_particle * belief_.size() + connection_list.connections_[i].num_particles * belief_.size(); i_sampl+=2)
                {
                    belief_.location(i_sampl).component(connection_list.connections_[i].part).position() = pose_0.orientation().rotation_matrix().transpose() * (pose_1.orientation().rotation_matrix() * (belief_.location(i_sampl).component(connection_list.connections_[i].relative_part).position() + belief_.location(i_sampl).component(connection_list.connections_[i].relative_part).orientation().rotation_matrix() * connection_list.connections_[i].relative_pose.translation()) + pose_1.position() - pose_0.position());

                    osr::FreeFloatingRigidBodiesState<> tempPoseArray;
                    tempPoseArray.recount(1);
                    tempPoseArray.setZero();
                    tempPoseArray.component(0).orientation().rotation_matrix(connection_list.connections_[i].relative_pose.rotation());
                    belief_.location(i_sampl).component(connection_list.connections_[i].part).orientation() = pose_0.orientation().inverse() * pose_1.orientation() * belief_.location(i_sampl).component(connection_list.connections_[i].relative_part).orientation() * tempPoseArray.component(0).orientation();

                    if (connection_list.connections_[i].part > connection_list.connections_[i].relative_part)
                        check_intersections_[connection_list.connections_[i].part][connection_list.connections_[i].relative_part] = false;
                    else
                        check_intersections_[connection_list.connections_[i].relative_part][connection_list.connections_[i].part] = false;
                }
            }

            for (int i = connection_list.connections_.size()-1; i >= 0; i--)
            {
                auto pose_0 = sensor_->integrated_poses().component(connection_list.connections_[i].part);
                auto pose_1 = sensor_->integrated_poses().component(connection_list.connections_[i].relative_part);
                for (size_t i_sampl = connection_list.connections_[i].first_particle * belief_.size()+1; i_sampl < connection_list.connections_[i].first_particle * belief_.size() + connection_list.connections_[i].num_particles * belief_.size(); i_sampl+=2)
                {
                    osr::FreeFloatingRigidBodiesState<> tempPoseArray;
                    tempPoseArray.recount(1);
                    tempPoseArray.setZero();
                    tempPoseArray.component(0).orientation().rotation_matrix(connection_list.connections_[i].relative_pose.rotation());
                    belief_.location(i_sampl).component(connection_list.connections_[i].relative_part).orientation() = pose_1.orientation().inverse() * pose_0.orientation() * belief_.location(i_sampl).component(connection_list.connections_[i].part).orientation() * tempPoseArray.component(0).orientation().inverse();

                    belief_.location(i_sampl).component(connection_list.connections_[i].relative_part).position() = pose_1.orientation().rotation_matrix().transpose() * (pose_0.orientation().rotation_matrix() * (belief_.location(i_sampl).component(connection_list.connections_[i].part).position() + belief_.location(i_sampl).component(connection_list.connections_[i].part).orientation().rotation_matrix() * (-(tempPoseArray.component(0).orientation().rotation_matrix().transpose()*connection_list.connections_[i].relative_pose.translation()))) + pose_0.position() - pose_1.position());

                    if (connection_list.connections_[i].part > connection_list.connections_[i].relative_part)
                        check_intersections_[connection_list.connections_[i].part][connection_list.connections_[i].relative_part] = false;
                    else
                        check_intersections_[connection_list.connections_[i].relative_part][connection_list.connections_[i].part] = false;
                }
            }

            // compute likelihood ----------------------------------------------
            bool update = (i_block == sampling_blocks_.size() - 1);
            RealArray new_loglikes = sensor_->loglikes(
                belief_.locations(), indices_, check_intersections_, update);

            // update the weights and resample if necessary --------------------
            belief_.delta_log_prob_mass(new_loglikes - loglikes_);
            loglikes_ = new_loglikes;
            if (belief_.kl_given_uniform() > max_kl_divergence_)
            {
                resample(belief_.size());
            }
        }
    }


    void resample(const size_t& sample_count)
    {
        IntArray indices(sample_count);
        std::vector<Noise> noises(sample_count);
        StateArray next_samples(sample_count);
        RealArray loglikes(sample_count);

        Belief new_belief(sample_count);

        for (size_t i = 0; i < sample_count; i++)
        {
            int index;
            new_belief.location(i) = belief_.sample(index);

            indices[i] = indices_[index];
            noises[i] = noises_[index];
            next_samples[i] = old_particles_[index];
            loglikes[i] = loglikes_[index];
        }
        belief_ = new_belief;
        indices_ = indices;
        noises_ = noises;
        old_particles_ = next_samples;
        loglikes_ = loglikes;
    }

    /// accessors **************************************************************
    std::vector<std::vector<int>> sampling_blocks() const
    {
        return sampling_blocks_;
    }

    void set_sampling_blocks(std::vector<std::vector<int>> sampling_blocks)
    {
        sampling_blocks_ = sampling_blocks;
    }

    /// mutators ***************************************************************
    Belief& belief() { return belief_; }
    void set_particles(const std::vector<State>& samples)
    {
        belief_.set_uniform(samples.size());
        for (int i = 0; i < belief_.size(); i++)
            belief_.location(i) = samples[i];

        indices_ = IntArray::Zero(belief_.size());
        loglikes_ = RealArray::Zero(belief_.size());
        noises_ = std::vector<Noise>(
            belief_.size(), Noise::Zero(transition_->noise_dimension()));
        old_particles_ = belief_.locations();

        sensor_->reset();
    }

    std::shared_ptr<Sensor> sensor()
    {
        return sensor_;
    }

    std::shared_ptr<Transition> transition()
    {
        return transition_;
    }

    std::vector<float> occlusion_map()
    {
        RealArray likelihoods = (loglikes_ - loglikes_.maxCoeff()).exp();
        fl::Real sum = likelihoods.sum();
        // normalize
        likelihoods /= sum;

        std::vector<float> new_occlusions = sensor_->Occlusions(0);
        std::vector<float> total_occlusions(new_occlusions.size(), .0);
        for (int pixel = 0; pixel < new_occlusions.size(); pixel++)
        {
            total_occlusions[pixel] += new_occlusions[pixel] * likelihoods[0];
        }
        for (int i = 1; i < belief_.size(); i++)
        {
            new_occlusions = sensor_->Occlusions(i);
            for (int pixel = 0; pixel < new_occlusions.size(); pixel++)
            {
                total_occlusions[pixel] += new_occlusions[pixel] * likelihoods[i];
            }
        }
        return total_occlusions;
    }

private:
    /// member variables *******************************************************
    Belief belief_;
    IntArray indices_;

    std::vector<Noise> noises_;
    StateArray old_particles_;
    RealArray loglikes_;

    // models
    std::shared_ptr<Sensor> sensor_;
    std::shared_ptr<Transition> transition_;

    // parameters
    std::vector<std::vector<int>> sampling_blocks_;
    fl::Real max_kl_divergence_;

    // distribution for sampling
    fl::Gaussian<Eigen::Matrix<fl::Real, 1, 1>> unit_gaussian_;

    // for which pairs should intersections be checked
    std::vector<std::vector<bool>> check_intersections_;
    
};
}
