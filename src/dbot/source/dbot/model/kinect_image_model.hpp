/*
 * This is part of the Bayesian Object Tracking (bot),
 * (https://github.com/bayesian-object-tracking)
 *
 * Copyright (c) 2015 Max Planck Society,
 * 				 Autonomous Motion Department,
 * 			     Institute for Intelligent Systems
 *
 * This Source Code Form is subject to the terms of the GNU General Public
 * License License (GNU GPL). A copy of the license can be found in the LICENSE
 * file distributed with this source code.
 */

/*
 * This file implements a part of the algorithm published in:
 *
 * M. Wuthrich, P. Pastor, M. Kalakrishnan, J. Bohg, and S. Schaal.
 * Probabilistic Object Tracking using a Range Camera
 * IEEE Intl Conf on Intelligent Robots and Systems, 2013
 * http://arxiv.org/abs/1505.00241
 *
 */

/**
 * \file kinect_image_model.hpp
 * \author Manuel Wuthrich (manuel.wuthrich@gmail.com)
 */

#pragma once

#include <vector>
#include <memory>
#include <cmath>
#include <thread>
#include <mutex>

#include <Eigen/Core>

#include <osr/pose_vector.hpp>
#include <fl/util/assertions.hpp>
#include <dbot/traits.hpp>
#include <osr/free_floating_rigid_bodies_state.hpp>
#include <dbot/model/rao_blackwell_sensor.hpp>

#include <dbot/rigid_body_renderer.hpp>
#include <dbot/model/kinect_pixel_model.hpp>
#include <dbot/model/occlusion_model.hpp>

#include <igl/copyleft/cgal/intersect_other.h>

namespace dbot
{
// Forward declarations
template <typename Scalar, typename State, int OBJECTS>
class KinectImageModel;

namespace internal
{
/**
 * ImageSensorCPU distribution traits specialization
 * \internal
 */
template <typename Scalar, typename State, int OBJECTS>
struct Traits<KinectImageModel<Scalar, State, OBJECTS>>
{
    typedef RbSensor<State> SensorBase;
    typedef typename SensorBase::Observation Observation;

    typedef std::shared_ptr<dbot::RigidBodyRenderer> ObjectRendererPtr;
    typedef std::shared_ptr<dbot::KinectPixelModel>
        PixelSensorPtr;
    typedef std::shared_ptr<dbot::OcclusionModel>
        OcclusionModelPtr;
};
}

/**
 * \class ImageSensorCPU
 *
 * \ingroup distributions
 * \ingroup sensors
 */
template <typename Scalar, typename State, int OBJECTS = -1>
class KinectImageModel
    : public internal::Traits<
          KinectImageModel<Scalar, State>>::SensorBase
{
public:
    typedef internal::Traits<KinectImageModel<Scalar, State>>
        Traits;

    typedef typename Traits::SensorBase Base;
    typedef typename Traits::Observation Observation;
    typedef typename Traits::ObjectRendererPtr ObjectRendererPtr;
    typedef typename Traits::PixelSensorPtr PixelSensorPtr;
    typedef typename Traits::OcclusionModelPtr OcclusionModelPtr;

    typedef typename Base::StateArray StateArray;
    typedef typename Base::RealArray RealArray;
    typedef typename Base::IntArray IntArray;

    typedef typename Eigen::Transform<fl::Real, 3, Eigen::Affine> Affine;

    // TODO: DO WE NEED ALL OF THIS IN THE CONSTRUCTOR??
    KinectImageModel(
        const Eigen::Matrix3d& camera_matrix,
        const size_t& n_rows,
        const size_t& n_cols,
        const ObjectRendererPtr object_renderer,
        const PixelSensorPtr sensor,
        const OcclusionModelPtr occlusion_transition,
        const float& initial_occlusion,
        const double& delta_time,
        const double surface_params[3],
        const int thread_count,
        const bool collision_checking)
        : camera_matrix_(camera_matrix),
          n_rows_(n_rows),
          n_cols_(n_cols),
          initial_occlusion_(initial_occlusion),
          object_model_(object_renderer),
          sensor_(sensor),
          occlusion_transition_(occlusion_transition),
          observation_time_(0),
          Base(delta_time),
          N_threads_(thread_count),
          collision_checking_(collision_checking)
    {
        surface_params_[0] = surface_params[0];
        surface_params_[1] = surface_params[1];
        surface_params_[2] = surface_params[2];

        static_assert_base(State, osr::RigidBodiesState<OBJECTS>);

        this->default_poses_.recount(object_model_->vertices().size());
        this->default_poses_.setZero();

        reset();
        
        for (size_t i = 0; i < object_model_->vertices_.size(); i++)
        {
            std::cout << "Constructing V and F of model: " << i << "(" << object_model_->vertices_[i].size() << " vertices)\n";
            Eigen::MatrixXd V_temp(object_model_->vertices_[i].size(), 3);
            Eigen::MatrixXi F_temp(object_model_->indices_[i].size(), 3);
            for (size_t j = 0; j < object_model_->vertices_[i].size(); j++)
                V_temp.row(j) = object_model_->vertices_[i][j];
            for (size_t j = 0; j < object_model_->indices_[i].size(); j++)
                F_temp.row(j) = Eigen::Vector3i(object_model_->indices_[i][j].data());
            V_.push_back(V_temp);
            V_transformed_.push_back(V_temp);
            F_.push_back(F_temp);
        }
    }

    virtual ~KinectImageModel() noexcept {}

    // returns depth of tabletop intersection
    Scalar dist_from_table()
    {
        double result = std::numeric_limits<double>::infinity();
        double point_dist_from_table;
        for (size_t i_obj = 0; i_obj < object_model_->vertices_.size(); i_obj++)
        {
            for (size_t i_vert = 0; i_vert < object_model_->vertices_[i_obj].size(); i_vert++)
            {
                Eigen::Vector3d trans_vertices;
                trans_vertices = V_transformed_[i_obj].row(i_vert);
                double p1 = trans_vertices(0);
                double p2 = trans_vertices(1);
                double p3 = trans_vertices(2);
                point_dist_from_table = surface_params_[0]*p1-(p2-surface_params_[1])+surface_params_[2]*p3;
                result = point_dist_from_table < result ? point_dist_from_table : result;
            }
        }
        return Scalar(result);
    }

    bool parts_intersect()
    {
        for (size_t i = 0; i < object_model_->vertices_.size(); i++)
        {
            for (size_t j = 0; j < i; j++)
            {
                Eigen::MatrixXi IF;
                if (igl::copyleft::cgal::intersect_other(V_transformed_[i],F_[i],V_transformed_[j],F_[j],true,IF)) return true;
            }
        }
        return false;
    }

    bool parts_intersect(std::vector<std::vector<bool>>& check_intersections, std::vector<Eigen::MatrixXd> V_transformed)
    {
        for (size_t i = 0; i < object_model_->vertices_.size(); i++)
        {
            for (size_t j = 0; j < i; j++)
            {
                if (check_intersections[i][j])
                {
                    Eigen::MatrixXi IF;
                    if (igl::copyleft::cgal::intersect_other(V_transformed[i], F_[i], V_transformed[j], F_[j],true,IF)) return true;
                }
            }
        }
        return false;
    }


    void loglikes_single_thread(const StateArray& deltas,
                                IntArray& indices,
                                std::vector<std::vector<bool>>& check_intersections,
                                const bool& update,
                                std::vector<std::vector<float>>& new_occlusions,
                                std::vector<std::vector<double>>& new_occlusion_times,
                                RealArray& log_likes,
                                size_t thread_id)
    {
        for (size_t i_state = size_t(deltas.size())*thread_id/N_threads_; i_state < size_t(deltas.size())*(thread_id+1)/N_threads_; i_state++)
        {
            if (update)
            {
                new_occlusions[i_state] = occlusions_[indices[i_state]];
                new_occlusion_times[i_state] =
                    occlusion_times_[indices[i_state]];
            }

            // render the object model -----------------------------------------
            int body_count = deltas[i_state].count();
            std::vector<Affine> poses(body_count);
            // for each mesh
            for (size_t i_obj = 0; i_obj < body_count; i_obj++)
            {
                auto pose_0 = this->default_poses_.component(i_obj);
                auto delta = deltas[i_state].component(i_obj);

                osr::PoseVector pose;

                /// \todo: this should be done through the the apply_delta
                /// function
                pose.position() = pose_0.orientation().rotation_matrix()
                        * delta.position() + pose_0.position();
                pose.orientation() = pose_0.orientation() * delta.orientation();
                poses[i_obj] = pose.affine();
            }
            object_models_[thread_id]->set_poses(poses);
            std::vector<int> intersect_indices;
            std::vector<float> predictions;
            object_models_[thread_id]->Render(camera_matrix_,
                                  n_rows_,
                                  n_cols_,
                                  intersect_indices,
                                  predictions);

            if (collision_checking_)
            {
                std::vector<Eigen::MatrixXd> V_transformed = V_;
                for (size_t i = 0; i < object_models_[thread_id]->vertices_.size(); i++)
                {
                    for (size_t i_vert = 0; i_vert < object_models_[thread_id]->vertices_[i].size(); i_vert++)
                    {
                        V_transformed[i].row(i_vert) = object_models_[thread_id]->R_[i] * object_models_[thread_id]->vertices_[i][i_vert] + object_models_[thread_id]->t_[i];
                    }
                }
                Scalar parts_intersect_coeff = parts_intersect(check_intersections, V_transformed) ? 0.01 : 1.0;
                log_likes[i_state] += log(parts_intersect_coeff);
            }

            // compute likelihoods (for each pixel)-----------------------------
            for (size_t i = 0; i < size_t(predictions.size()); i++)
            {
                if (std::isnan(observations_[intersect_indices[i]]))
                {
                    log_likes[i_state] += log(1.);
                }
                else
                {
                    double delta_time = observation_time_ -
                                        occlusion_times_[indices[i_state]]
                                                        [intersect_indices[i]];

                    occlusion_transitions_[thread_id]->Condition(
                        delta_time,
                        occlusions_[indices[i_state]][intersect_indices[i]]);

                    float occlusion =
                        occlusion_transitions_[thread_id]->MapStandardGaussian();

                    sensors_[thread_id]->Condition(predictions[i], false);
                    float p_obsIpred_vis =
                        sensors_[thread_id]->Probability(
                            observations_[intersect_indices[i]]) *
                        (1.0 - occlusion);

                    sensors_[thread_id]->Condition(predictions[i], true);
                    float p_obsIpred_occl =
                        sensors_[thread_id]->Probability(
                            observations_[intersect_indices[i]]) *
                        occlusion;

                    sensors_[thread_id]->Condition(
                        std::numeric_limits<float>::infinity(), true);
                    float p_obsIinf = sensors_[thread_id]->Probability(
                        observations_[intersect_indices[i]]);

                    log_likes[i_state] +=
                        log((p_obsIpred_vis + p_obsIpred_occl)/ p_obsIinf);

                    // we update the occlusion with the observations
                    if (update)
                    {
                        new_occlusions[i_state][intersect_indices[i]] =
                            p_obsIpred_occl /
                            (p_obsIpred_vis + p_obsIpred_occl);
                        new_occlusion_times[i_state][intersect_indices[i]] =
                            observation_time_;
                    }
                }
            }
        }
    }


    RealArray loglikes(const StateArray& deltas,
                       IntArray& indices,
                       std::vector<std::vector<bool>>& check_intersections,
                       const bool& update = false)
    {
        std::vector<std::vector<float>> new_occlusions(deltas.size());
        std::vector<std::vector<double>> new_occlusion_times(deltas.size());

        RealArray log_likes = RealArray::Zero(deltas.size());

        //std::vector<RealArray> log_likes_singles(N_threads_, log_likes);
        //std::vector<std::vector<std::vector<float>>> new_occlusions_singles(N_threads_, new_occlusions);
        //std::vector<std::vector<std::vector<double>>> new_occlusion_times_singles(N_threads_, new_occlusion_times);

        object_models_.clear();
        sensors_.clear();
        occlusion_transitions_.clear();

        for (size_t i = 0; i < N_threads_; i++)
        {
            auto new_rigid_body_renderer = std::make_shared<RigidBodyRenderer>(*object_model_);
            object_models_.push_back(new_rigid_body_renderer);
            auto new_sensor = std::make_shared<KinectPixelModel>(*sensor_);
            sensors_.push_back(new_sensor);
            auto new_occlusion_transition = std::make_shared<OcclusionModel>(*occlusion_transition_);
            occlusion_transitions_.push_back(new_occlusion_transition);
        }
        std::vector<std::thread> threads;
        for (size_t i_thread = 0; i_thread < N_threads_; i_thread++)
        {
            threads.push_back(std::thread(&KinectImageModel::loglikes_single_thread,
                                          this,
                                          std::ref(deltas),
                                          std::ref(indices),
                                          std::ref(check_intersections),
                                          std::ref(update),
                                          std::ref(new_occlusions),//_singles[i_thread]),
                                          std::ref(new_occlusion_times),//_singles[i_thread]),
                                          std::ref(log_likes),//_singles[i_thread]),
                                          i_thread));
        }
        for (size_t i_thread = 0; i_thread < N_threads_; i_thread++)
        {
            threads[i_thread].join();
            /*for (size_t i_state = size_t(deltas.size())*i_thread/N_threads_; i_state < size_t(deltas.size())*(i_thread+1)/N_threads_; i_state++)
            {
                log_likes[i_state] = log_likes_singles[i_thread][i_state];
                new_occlusions[i_state] = new_occlusions_singles[i_thread][i_state];
                new_occlusion_times[i_state] = new_occlusion_times_singles[i_thread][i_state];
            }*/
        }

        if (update)
        {
            occlusions_ = new_occlusions;
            occlusion_times_ = new_occlusion_times;
            for (size_t i_state = 0; i_state < indices.size(); i_state++)
                indices[i_state] = i_state;
        }
        return log_likes;
    }

    RealArray loglikes(const StateArray& deltas,
                       IntArray& indices,
                       const bool& update = false)
    {
        std::vector<std::vector<float>> new_occlusions(deltas.size());
        std::vector<std::vector<double>> new_occlusion_times(deltas.size());

        RealArray log_likes = RealArray::Zero(deltas.size());
        for (size_t i_state = 0; i_state < size_t(deltas.size()); i_state++)
        {
            if (update)
            {
                new_occlusions[i_state] = occlusions_[indices[i_state]];
                new_occlusion_times[i_state] =
                    occlusion_times_[indices[i_state]];
            }

            // render the object model -----------------------------------------
            int body_count = deltas[i_state].count();
            std::vector<Affine> poses(body_count);
            // for each mesh
            for (size_t i_obj = 0; i_obj < body_count; i_obj++)
            {
                auto pose_0 = this->default_poses_.component(i_obj);
                auto delta = deltas[i_state].component(i_obj);

                osr::PoseVector pose;

                /// \todo: this should be done through the the apply_delta
                /// function
                pose.position() = pose_0.orientation().rotation_matrix()
                        * delta.position() + pose_0.position();
                pose.orientation() = pose_0.orientation() * delta.orientation();

                poses[i_obj] = pose.affine();
            }
            object_model_->set_poses(poses);
            std::vector<int> intersect_indices;
            std::vector<float> predictions;
            object_model_->Render(camera_matrix_,
                                  n_rows_,
                                  n_cols_,
                                  intersect_indices,
                                  predictions);

            for (size_t i = 0; i < object_model_->vertices_.size(); i++)
            {
                for (size_t i_vert = 0; i_vert < object_model_->vertices_[i].size(); i_vert++)
                {
                    V_transformed_[i].row(i_vert) = object_model_->R_[i] * object_model_->vertices_[i][i_vert] + object_model_->t_[i];
                }
            }
            //Scalar distance_from_table = dist_from_table();
            //Scalar above_table_coeff = distance_from_table > 0 ? 1.0 : std::exp(distance_from_table*1000);
            //Scalar parts_intersect_coeff = parts_intersect() ? 0.01 : 1.0;
            //log_likes[i_state] += log(above_table_coeff * parts_intersect_coeff);

            // compute likelihoods (for each pixel)-----------------------------
            for (size_t i = 0; i < size_t(predictions.size()); i++)
            {
                if (std::isnan(observations_[intersect_indices[i]]))
                {
                    log_likes[i_state] += log(1.);
                }
                else
                {
                    double delta_time = observation_time_ -
                                        occlusion_times_[indices[i_state]]
                                                        [intersect_indices[i]];

                    occlusion_transition_->Condition(
                        delta_time,
                        occlusions_[indices[i_state]][intersect_indices[i]]);

                    float occlusion =
                        occlusion_transition_->MapStandardGaussian();

                    sensor_->Condition(predictions[i], false);
                    float p_obsIpred_vis =
                        sensor_->Probability(
                            observations_[intersect_indices[i]]) *
                        (1.0 - occlusion);

                    sensor_->Condition(predictions[i], true);
                    float p_obsIpred_occl =
                        sensor_->Probability(
                            observations_[intersect_indices[i]]) *
                        occlusion;

                    sensor_->Condition(
                        std::numeric_limits<float>::infinity(), true);
                    float p_obsIinf = sensor_->Probability(
                        observations_[intersect_indices[i]]);

                    log_likes[i_state] +=
                        log((p_obsIpred_vis + p_obsIpred_occl)/ p_obsIinf);

                    // we update the occlusion with the observations
                    if (update)
                    {
                        new_occlusions[i_state][intersect_indices[i]] =
                            p_obsIpred_occl /
                            (p_obsIpred_vis + p_obsIpred_occl);
                        new_occlusion_times[i_state][intersect_indices[i]] =
                            observation_time_;
                    }
                }
            }
        }
        if (update)
        {
            occlusions_ = new_occlusions;
            occlusion_times_ = new_occlusion_times;
            for (size_t i_state = 0; i_state < indices.size(); i_state++)
                indices[i_state] = i_state;
        }
        return log_likes;
    }

    void set_observation(const Observation& image)
    {
        assert(image.rows() == image.size());
        assert(image.cols() == 1);

        std::vector<float> std_measurement(image.size());        

        for (int i = 0; i < image.size(); ++i)
        {
            std_measurement[i] = image(i, 0);
        }

        set_observation(std_measurement, this->delta_time_);
    }

    virtual void reset()
    {
        occlusions_.resize(1);
        occlusions_[0] =
            std::vector<float>(n_rows_ * n_cols_, initial_occlusion_);
        occlusion_times_.resize(1);
        occlusion_times_[0] = std::vector<double>(n_rows_ * n_cols_, 0);
        observation_time_ = 0;
    }

    // TODO: TYPES
    const std::vector<float> Occlusions(size_t index)
    {
        return occlusions_[index];
    }

    const int num_objects()
    {
        return object_model_->vertices_.size();
    } 

private:
    void set_observation(const std::vector<float>& observations,
                         const Scalar& delta_time)
    {
        observations_ = observations;
        observation_time_ += delta_time;
    }

    const size_t N_threads_;
    std::mutex t_mutex;
    std::vector<ObjectRendererPtr> object_models_;
    std::vector<PixelSensorPtr> sensors_;
    std::vector<OcclusionModelPtr> occlusion_transitions_;

    // TODO: WE PROBABLY DONT NEED ALL OF THIS
    const Eigen::Matrix3d camera_matrix_;
    const size_t n_rows_;
    const size_t n_cols_;
    const float initial_occlusion_;
    const std::shared_ptr<osr::RigidBodiesState<-1>> rigid_bodies_state_;

    // models
    ObjectRendererPtr object_model_;
    PixelSensorPtr sensor_;
    OcclusionModelPtr occlusion_transition_;

    // occlusion parameters
    std::vector<std::vector<float>> occlusions_;
    std::vector<std::vector<double>> occlusion_times_;

    // observed data
    std::vector<float> observations_;
    double observation_time_;

    bool collision_checking_;
    bool surface_params_received_ = false;
    double surface_params_[3];
    std::vector<Eigen::MatrixXd> V_;
    std::vector<Eigen::MatrixXd> V_transformed_;
    std::vector<Eigen::MatrixXi> F_;
};
}
