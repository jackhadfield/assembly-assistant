/*
 * This is part of the Bayesian Object Tracking (bot),
 * (https://github.com/bayesian-object-tracking)
 *
 * Copyright (c) 2015 Max Planck Society,
 * 				 Autonomous Motion Department,
 * 			     Institute for Intelligent Systems
 *
 * This Source Code Form is subject to the terms of the GNU General Public
 * License License (GNU GPL). A copy of the license can be found in the LICENSE
 * file distributed with this source code.
 */

/*
 * This file implements a part of the algorithm published in:
 *
 * M. Wuthrich, P. Pastor, M. Kalakrishnan, J. Bohg, and S. Schaal.
 * Probabilistic Object Tracking using a Range Camera
 * IEEE Intl Conf on Intelligent Robots and Systems, 2013
 * http://arxiv.org/abs/1505.00241
 *
 */

#include <dbot/tracker/particle_tracker.hpp>

namespace dbot
{
ParticleTracker::ParticleTracker(
    const std::shared_ptr<Filter>& filter,
    const std::shared_ptr<ObjectModel>& object_model,
    int evaluation_count,
    double update_rate,
    bool center_object_frame)
    : Tracker(object_model, update_rate, center_object_frame),
      filter_(filter),
      evaluation_count_(evaluation_count)
{
}

auto ParticleTracker::on_initialize(
    const std::vector<State>& initial_states) -> State
{
    filter_->set_particles(initial_states);
    filter_->resample(evaluation_count_ / filter_->sampling_blocks().size());

    State delta_mean = filter_->belief().mean();

    for (size_t i = 0; i < filter_->belief().size(); i++)
    {
        filter_->belief().location(i).subtract(delta_mean);
    }

    auto& integrated_poses = filter_->sensor()->integrated_poses();
    integrated_poses.apply_delta(delta_mean);

    return integrated_poses;
}

auto ParticleTracker::on_track(const Obsrv& image) -> State
{
    filter_->filter(image, zero_input());

    State delta_mean = filter_->belief().mean();

    for (size_t i = 0; i < filter_->belief().size(); i++)
    {
        filter_->belief().location(i).subtract(delta_mean);
    }

    auto& integrated_poses = filter_->sensor()->integrated_poses();
    integrated_poses.apply_delta(delta_mean);

    return integrated_poses;
}

auto ParticleTracker::on_track(const Obsrv& image, const ConnectionInfoList& connection_list) -> State
{

    filter_->filter(image, zero_input(), connection_list);

    State delta_mean = filter_->belief().mean();

    for (size_t i = 0; i < filter_->belief().size(); i++)
    {
        filter_->belief().location(i).subtract(delta_mean);
    }

    auto& integrated_poses = filter_->sensor()->integrated_poses();
    integrated_poses.apply_delta(delta_mean);

    return integrated_poses;
}

auto ParticleTracker::on_track(const Obsrv& image, const Input& input, const ConnectionInfoList& connection_list) -> State
{

    filter_->filter(image, input, connection_list);

    State delta_mean = filter_->belief().mean();
    for (size_t i = 0; i < filter_->belief().size(); i++)
    {
        filter_->belief().location(i).subtract(delta_mean);
    }

    auto& integrated_poses = filter_->sensor()->integrated_poses();
    integrated_poses.apply_delta(delta_mean);

    return integrated_poses;
}

auto ParticleTracker::on_track(const Obsrv& image, const Input& input, const ConnectionInfoList& connection_list, const Eigen::Matrix<double, -1, 1>& input_scores) -> State
{

    filter_->filter(image, input, connection_list, input_scores);
    State delta_mean = filter_->belief().mean();
    for (size_t i = 0; i < filter_->belief().size(); i++)
    {
        filter_->belief().location(i).subtract(delta_mean);
    }

    auto& integrated_poses = filter_->sensor()->integrated_poses();
    integrated_poses.apply_delta(delta_mean);

    return integrated_poses;
}

auto ParticleTracker::on_track(const Obsrv& image, const Input& input, const ConnectionInfoList& connection_list, const Eigen::Matrix<double, -1, 1>& input_scores, std::vector<float>& occlusion_map) -> State
{

    filter_->filter(image, input, connection_list, input_scores);

    State delta_mean = filter_->belief().mean();
    for (size_t i = 0; i < filter_->belief().size(); i++)
    {
        filter_->belief().location(i).subtract(delta_mean);
    }

    auto& integrated_poses = filter_->sensor()->integrated_poses();
    integrated_poses.apply_delta(delta_mean);

    occlusion_map = filter_->occlusion_map();

    return integrated_poses;
}

auto ParticleTracker::on_track(const Obsrv& image, const Input& input, const ConnectionInfoList& connection_list, Eigen::Matrix<double, -1, 1>& input_scores, const std::vector<int>& active_ids) -> State
{
    bool active_id_change = false;
    if (active_ids.size() != previous_active_ids_.size())
        active_id_change = true;
    else
        for (int i = 0; i < active_ids.size(); i++)
            if (active_ids[i] != previous_active_ids_[i])
                active_id_change = true;
    if (active_ids.size() > 0)
    {
        if (active_id_change)
        {
            std::vector<std::vector<int>> sampling_blocks(active_ids.size());
            for (int i = 0; i < active_ids.size(); i++)
            {
                for (int k = 0; k < filter_->transition()->noise_dimension()/object_model_->count_parts(); ++k)
                {
                    sampling_blocks[i].push_back(active_ids[i] * filter_->transition()->noise_dimension()/object_model_->count_parts() + k);
                }
            }
            filter_->set_sampling_blocks(sampling_blocks);
            filter_->resample(evaluation_count_ / filter_->sampling_blocks().size());
            int ind = 0;
            for (int i_obj = 0; i_obj < object_model_->count_parts(); i_obj++)
            {
                if (i_obj == active_ids[ind])
                {
                    ind++;
                }
                else
                {
                    for (size_t i_sampl = 0; i_sampl < filter_->belief().size(); i_sampl++)
                    {
                        filter_->belief().location(i_sampl).component(i_obj).position().setZero();
                        filter_->belief().location(i_sampl).component(i_obj).orientation().setZero();
                        filter_->belief().location(i_sampl).component(i_obj).linear_velocity().setZero();
                        filter_->belief().location(i_sampl).component(i_obj).angular_velocity().setZero();
                    }
                }
            }
        }

        int ind = 0;
        for (int i_obj = 0; i_obj < object_model_->count_parts(); i_obj++)
        {
            if (i_obj == active_ids[ind])
            {
                ind++;
            }
            else
            {
                input_scores(3*i_obj) = 0;
                input_scores(3*i_obj + 1) = 0;
                input_scores(3*i_obj + 2) = 0;
            }
        }

        filter_->filter(image, input, connection_list, input_scores);

        State delta_mean = filter_->belief().mean();
        for (size_t i = 0; i < filter_->belief().size(); i++)
        {
            filter_->belief().location(i).subtract(delta_mean);
        }

        auto& integrated_poses = filter_->sensor()->integrated_poses();
        integrated_poses.apply_delta(delta_mean);
        previous_active_ids_ = active_ids;
        return integrated_poses;
    }
    else
    {
        previous_active_ids_ = active_ids;
        return filter_->sensor()->integrated_poses();
    }
}

}
