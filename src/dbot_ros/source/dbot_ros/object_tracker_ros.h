/*
 * This is part of the Bayesian Object Tracking (bot),
 * (https://github.com/bayesian-object-tracking)
 *
 * Copyright (c) 2015 Max Planck Society,
 * 				 Autonomous Motion Department,
 * 			     Institute for Intelligent Systems
 *
 * This Source Code Form is subject to the terms of the GNU General Public
 * License License (GNU GPL). A copy of the license can be found in the LICENSE
 * file distributed with this source code.
 */

/**
 * \file ros_object_tracker.h
 * \date Januray 2016
 * \author Jan Issac (jan.issac@gmail.com)
 */

#pragma once

#include <vector>
#include <memory>
#include <mutex>
#include <dbot/camera_data.hpp>
#include <dbot/connection_info.hpp>
#include <sensor_msgs/Image.h>
#include <geometry_msgs/PoseStamped.h>
#include <dbot_ros_msgs/ObjectState.h>
#include <object_assembly_msgs/Input.h>
#include <object_assembly_msgs/ConnectionInfoList.h>
#include <ros/ros.h>
#include <ros/package.h>

namespace dbot
{
/**
 * \brief Represents a generic tracker node
 */
template <typename Tracker>
class ObjectTrackerRos
{
public:
    typedef typename Tracker::State State;
    typedef typename Tracker::Obsrv Obsrv;
    typedef typename Tracker::Input Input;

public:
    /**
     * \brief Creates a ObjectTrackerRos
     */
    ObjectTrackerRos(const std::shared_ptr<Tracker>& tracker,
                     const std::shared_ptr<dbot::CameraData>& camera_data,
                     int object_count,
                     bool use_input,
                     int input_type,
                     bool use_connection_list,
                     float z_input_weight);
    
    /**
     * \brief TODO
     */
    void get_active_ids(const sensor_msgs::Image& ros_image,
                        const object_assembly_msgs::Input& input_msg,
                        const ConnectionInfoList& connection_info_list,
                        std::vector<int>& active_ids);

    /**
     * \brief Tracking callback function which is invoked whenever a new image
     *        is available
     */
    void track(const sensor_msgs::Image& ros_image);

    /**
     * \brief Tracking callback function which is invoked whenever a new image
     *        is available and connections are enabled
     */
    void track(const sensor_msgs::Image& ros_image,
               const object_assembly_msgs::ConnectionInfoList&
                     connection_list_msg);

    /**
     * \brief Tracking callback function which is invoked whenever a new image
     *        is available and input and connections are enabled
     */
    void track(const sensor_msgs::Image& ros_image,
               const object_assembly_msgs::Input& input_msg,
               const object_assembly_msgs::ConnectionInfoList&
                     connection_list_msg);

    void initialize(const std::vector<State>& initial_states);

    /**
     * \brief Incoming observation callback function
     * \param ros_image new observation
     */
    void update_obsrv(const sensor_msgs::Image& ros_image);

    /**
     * \brief Incoming input callback function
     * \param input new input
     */
    void update_input(const object_assembly_msgs::Input& input);

    void update_connection_list(
        const object_assembly_msgs::ConnectionInfoList&
            connection_list_msg);

    void run();
    bool run_once();

    State current_state() const;
    geometry_msgs::PoseStamped current_pose() const;
    std::vector<dbot_ros_msgs::ObjectState> current_state_messages() const;
    std::vector<geometry_msgs::PoseStamped> current_poses() const;

    const std::shared_ptr<Tracker>& tracker() { return tracker_; }

    void shutdown();

protected:
    bool obsrv_updated_;
    bool running_;
    State current_state_;
    double current_time_;
    int object_count_;
    std::vector<geometry_msgs::PoseStamped> current_poses_;
    std::vector<geometry_msgs::PoseStamped> current_velocities_;
    sensor_msgs::Image current_ros_image_;
    std::mutex obsrv_mutex_;
    std::shared_ptr<Tracker> tracker_;
    std::shared_ptr<dbot::CameraData> camera_data_;

    bool input_updated_;
    bool use_input_;
    bool use_connection_list_;
    bool first_iteration_;
    bool selective_tracking_ = false; //TODO
    int input_type_;
    std::mutex input_mutex_;
    object_assembly_msgs::Input current_input_;
    float z_input_weight_;

    bool connection_list_received_;
    std::mutex connection_list_mutex_;
    object_assembly_msgs::ConnectionInfoList current_connection_list_;
    ros::NodeHandle node_handle_;
    ros::Publisher occ_image_publisher_;

    int tmp_counter_=0;
};


}
