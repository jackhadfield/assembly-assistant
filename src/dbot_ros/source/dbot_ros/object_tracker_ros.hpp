/*
 * This is part of the Bayesian Object Tracking (bot),
 * (https://github.com/bayesian-object-tracking)
 *
 * Copyright (c) 2015 Max Planck Society,
 * 				 Autonomous Motion Department,
 * 			     Institute for Intelligent Systems
 *
 * This Source Code Form is subject to the terms of the GNU General Public
 * License License (GNU GPL). A copy of the license can be found in the LICENSE
 * file distributed with this source code.
 */

/**
 * \file tracker_node.hpp
 * \date Januray 2016
 * \author Jan Issac (jan.issac@gmail.com)
 */

#pragma once

#include <fl/util/profiling.hpp>

#include <dbot_ros/object_tracker_ros.h>

#include <dbot_ros/util/ros_interface.hpp>
#include <dbot/connection_info.hpp>

namespace dbot
{
template <typename Tracker>
ObjectTrackerRos<Tracker>::ObjectTrackerRos(
    const std::shared_ptr<Tracker>& tracker,
    const std::shared_ptr<dbot::CameraData>& camera_data,
    int object_count,
    bool use_input,
    int input_type,
    bool use_connection_list,
    float z_input_weight)
    : tracker_(tracker),
      camera_data_(camera_data),
      object_count_(object_count),
      obsrv_updated_(false),
      running_(false),
      use_input_(use_input),
      input_type_(input_type),
      use_connection_list_(use_connection_list),
      z_input_weight_(z_input_weight),
      input_updated_(false),
      first_iteration_(true),
      connection_list_received_(false),
      node_handle_("~")
{
    occ_image_publisher_ = node_handle_.advertise<sensor_msgs::Image>("occlusion_image", 0);
}

template <typename Tracker>
void ObjectTrackerRos<Tracker>::get_active_ids(const sensor_msgs::Image& ros_image,
                    const object_assembly_msgs::Input& input_msg,
                    const ConnectionInfoList& connection_info_list,
                    std::vector<int>& active_ids)
{
    double fx = camera_data_->camera_matrix()(0, 0);
    double fy = camera_data_->camera_matrix()(1, 1);
    double cx = camera_data_->camera_matrix()(0, 2);
    double cy = camera_data_->camera_matrix()(1, 2);
    int num_hand_points = (input_msg.input.size())/4 - object_count_; //TODO camera_data_->downsampling_factor()
    std::vector<double> hand_points;
    for (int i = object_count_*4; i < input_msg.input.size() - num_hand_points; i+=3)
    {
        if (input_msg.input[i+0]>=0 && input_msg.input[i+1]>=0 && input_msg.input[i+0]<ros_image.width && input_msg.input[i+1]<ros_image.height)
        {
            int pt = (4*(int)(input_msg.input[i+0]) + 4*ros_image.width * (int)(input_msg.input[i+1]));
            float *p = (float*)(ros_image.data.data()+4*pt);
            if (*p < 0.05)
            {
                hand_points.push_back((input_msg.input[i+0] - cx)*input_msg.input[i+2]/fx);
                hand_points.push_back((input_msg.input[i+1] - cy)*input_msg.input[i+2]/fy);
                hand_points.push_back(input_msg.input[i+2]);
            }
            else
            {
                hand_points.push_back((input_msg.input[i+0] - cx)*(*p)/fx);
                hand_points.push_back((input_msg.input[i+1] - cy)*(*p)/fy);
                hand_points.push_back(*p);
            }
        }
        else
        {
            //hand_points.push_back(0);
            //hand_points.push_back(0);
            //hand_points.push_back(0);
        }
    }

    std::vector<bool> is_active(object_count_, false);
    for (int i = 0; i < object_count_; i++)
    {
        for (int j = 0; j < hand_points.size()/3; j++)
            if (std::pow(current_poses_[i].pose.position.x - hand_points[j+0], 2) +
                std::pow(current_poses_[i].pose.position.y - hand_points[j+1], 2) +
                std::pow(current_poses_[i].pose.position.z - hand_points[j+2], 2) < 0.09)
            {
                is_active[i] = true;
                break;
            }
    }

    for (auto conn: connection_info_list.connections_)
        if (conn.num_particles > 0.0)
        {
            if (is_active[conn.relative_part])
                is_active[conn.part] = true;
            if (is_active[conn.part])
                is_active[conn.relative_part] = true;
        }
    for (int i = 0; i < is_active.size(); i++)
        if (is_active[i]) active_ids.push_back(i);
}

template <typename Tracker>
void ObjectTrackerRos<Tracker>::track(const sensor_msgs::Image& ros_image)
{
    auto image = ri::to_eigen_vector<typename Obsrv::Scalar>(
        ros_image, camera_data_->downsampling_factor());

    current_poses_.clear();
    current_velocities_.clear();
    current_state_ = tracker_->track(image);
    geometry_msgs::PoseStamped current_pose;
    geometry_msgs::PoseStamped current_velocity;
    for (int i = 0; i < object_count_; ++i)
    {
        current_pose.pose = ri::to_ros_pose(current_state_.component(i));
        current_pose.header.stamp    = ros_image.header.stamp;
        current_pose.header.frame_id = ros_image.header.frame_id;

        current_poses_.push_back(current_pose);

        current_velocity.pose = ri::to_ros_velocity(current_state_.component(i));
        current_velocity.header.stamp    = ros_image.header.stamp;
        current_velocity.header.frame_id = ros_image.header.frame_id;

        current_velocities_.push_back(current_velocity);
    }
}

template <typename Tracker>
void ObjectTrackerRos<Tracker>::track(
        const sensor_msgs::Image& ros_image,
        const object_assembly_msgs::ConnectionInfoList&
              connection_list_msg)
{
    auto image = ri::to_eigen_vector<typename Obsrv::Scalar>(
        ros_image, camera_data_->downsampling_factor());

    current_poses_.clear();
    current_velocities_.clear();

    std::vector<ConnectionInfoList::ConnectionInfo> connections;
    for (int i = 0; i < connection_list_msg.connections.size(); i++)
    {
        ConnectionInfoList::ConnectionInfo temp_conn;
        temp_conn.part = connection_list_msg.connections[i].part;
        temp_conn.relative_part = connection_list_msg.connections[i].relative_part;
        temp_conn.num_particles = connection_list_msg.connections[i].num_particles;
        temp_conn.first_particle = connection_list_msg.connections[i].first_particle;
        Eigen::Vector3d translation;
        translation << connection_list_msg.connections[i].relative_pose.position.x, connection_list_msg.connections[i].relative_pose.position.y, connection_list_msg.connections[i].relative_pose.position.z;
        Eigen::Quaternion<double> q;
        q.x() = connection_list_msg.connections[i].relative_pose.orientation.x;
        q.y() = connection_list_msg.connections[i].relative_pose.orientation.y;
        q.z() = connection_list_msg.connections[i].relative_pose.orientation.z;
        q.w() = connection_list_msg.connections[i].relative_pose.orientation.w;
        temp_conn.relative_pose = Eigen::Translation3d(translation)*q;
        connections.push_back(temp_conn);
    }
    ConnectionInfoList connection_info_list(object_count_, connections);

    current_state_ = tracker_->track(image, connection_info_list);
    geometry_msgs::PoseStamped current_pose;
    geometry_msgs::PoseStamped current_velocity;
    for (int i = 0; i < object_count_; ++i)
    {
        current_pose.pose = ri::to_ros_pose(current_state_.component(i));
        current_pose.header.stamp    = ros_image.header.stamp;
        current_pose.header.frame_id = ros_image.header.frame_id;

        current_poses_.push_back(current_pose);

        current_velocity.pose = ri::to_ros_velocity(current_state_.component(i));
        current_velocity.header.stamp    = ros_image.header.stamp;
        current_velocity.header.frame_id = ros_image.header.frame_id;

        current_velocities_.push_back(current_velocity);
    }
}


template <typename Tracker>
void ObjectTrackerRos<Tracker>::track(
        const sensor_msgs::Image& ros_image,
        const object_assembly_msgs::Input& input_msg,
        const object_assembly_msgs::ConnectionInfoList&
              connection_list_msg)
{
    //ROS_INFO("Converting ROS messages for tracker\n");
    auto image = ri::to_eigen_vector<typename Obsrv::Scalar>(
        ros_image, camera_data_->downsampling_factor());
    Eigen::Matrix<double, -1, 1> input;
    Eigen::Matrix<double, -1, 1> input_scores;
    double fx = camera_data_->camera_matrix()(0, 0);
    double fy = camera_data_->camera_matrix()(1, 1);
    double cx = camera_data_->camera_matrix()(0, 2);
    double cy = camera_data_->camera_matrix()(1, 2);

    if (!first_iteration_)
    {
        input.resize(3*object_count_, 1);
        input_scores.resize(3*object_count_, 1);
        for (int i = 0; i < object_count_; ++i)
        {
            //Check if input points are within image bounds
            if (input_msg.input[3*i+0]>=0 && input_msg.input[3*i+1]>=0 && input_msg.input[3*i+0]<ros_image.width && input_msg.input[3*i+1]<ros_image.height)
            {
                float z = image((int)(input_msg.input[3*i+0]) + (ros_image.width/camera_data_->downsampling_factor()) * (int)(input_msg.input[3*i+1]), 0);
                float *p = &z;

                //Check if depth value exists
                if (*p > 0.05)
                {
                    input(3*i+0) = (input_msg.input[3*i+0] - cx)*(*p)/fx;
                    input(3*i+1) = (input_msg.input[3*i+1] - cy)*(*p)/fy;
                    input(3*i+2) = *p;
                }
                else
                {
                    input(3*i+0) = (input_msg.input[3*i+0] - cx)*input_msg.input[3*i+2]/fx;
                    input(3*i+1) = (input_msg.input[3*i+1] - cy)*input_msg.input[3*i+2]/fy;
                    input(3*i+2) = input_msg.input[3*i+2];
                }
                input_scores(3*i+0) = input_msg.input[3*object_count_+i];
                input_scores(3*i+1) = input_msg.input[3*object_count_+i];
                input_scores(3*i+2) = z_input_weight_ * input_msg.input[3*object_count_+i];
            }
            else
            {
                input(3*i+0) = 0;
                input(3*i+1) = 0;
                input(3*i+2) = 0;

                input_scores(3*i+0) = 0;
                input_scores(3*i+1) = 0;
                input_scores(3*i+2) = 0;
            }
        }
    }
    else
    {
        input.setZero(3*object_count_, 1);
        input_scores.setZero(3*object_count_, 1);
        first_iteration_ = false;
    }

    std::vector<ConnectionInfoList::ConnectionInfo> connections;
    for (int i = 0; i < connection_list_msg.connections.size(); i++)
    {
        ConnectionInfoList::ConnectionInfo temp_conn;
        temp_conn.part = connection_list_msg.connections[i].part;
        temp_conn.relative_part = connection_list_msg.connections[i].relative_part;
        temp_conn.num_particles = connection_list_msg.connections[i].num_particles;
        temp_conn.first_particle = connection_list_msg.connections[i].first_particle;
        Eigen::Vector3d translation;
        translation << connection_list_msg.connections[i].relative_pose.position.x, connection_list_msg.connections[i].relative_pose.position.y, connection_list_msg.connections[i].relative_pose.position.z;
        Eigen::Quaternion<double> q;
        q.x() = connection_list_msg.connections[i].relative_pose.orientation.x;
        q.y() = connection_list_msg.connections[i].relative_pose.orientation.y;
        q.z() = connection_list_msg.connections[i].relative_pose.orientation.z;
        q.w() = connection_list_msg.connections[i].relative_pose.orientation.w;
        temp_conn.relative_pose = Eigen::Translation3d(translation)*q;
        connections.push_back(temp_conn);
    }
    ConnectionInfoList connection_info_list(object_count_, connections);

    if (selective_tracking_)
    {
        std::vector<int> active_ids;
        get_active_ids(ros_image, input_msg, connection_info_list, active_ids);
        current_state_ = tracker_->track(image, input, connection_info_list, input_scores, active_ids);
    }
    else
        current_state_ = tracker_->track(image, input, connection_info_list, input_scores);

    current_poses_.clear();
    current_velocities_.clear();
    geometry_msgs::PoseStamped current_pose;
    geometry_msgs::PoseStamped current_velocity;
    for (int i = 0; i < object_count_; ++i)
    {
        current_pose.pose = ri::to_ros_pose(current_state_.component(i));
        current_pose.header.stamp    = ros_image.header.stamp;
        current_pose.header.frame_id = ros_image.header.frame_id;

        current_poses_.push_back(current_pose);

        current_velocity.pose = ri::to_ros_velocity(current_state_.component(i));
        current_velocity.header.stamp    = ros_image.header.stamp;
        current_velocity.header.frame_id = ros_image.header.frame_id;

        current_velocities_.push_back(current_velocity);
    }
}


template <typename Tracker>
void ObjectTrackerRos<Tracker>::update_obsrv(
    const sensor_msgs::Image& ros_image)
{
    std::lock_guard<std::mutex> lock_obsrv(obsrv_mutex_);

    if(obsrv_updated_)
    {
        ROS_INFO("An Image has been skipped because update was too slow!"
                 " Consider reducing cost of update, e.g. by reducing number"
                 " of particles");
    }

    current_ros_image_ = ros_image;
    obsrv_updated_     = true;
}

template <typename Tracker>
void ObjectTrackerRos<Tracker>::update_input(
    const object_assembly_msgs::Input& input_msg)
{
//TODO check input length and vals
    std::lock_guard<std::mutex> lock_input(input_mutex_);
    current_input_ = input_msg;
    input_updated_ = true;
}

template <typename Tracker>
void ObjectTrackerRos<Tracker>::update_connection_list(
    const object_assembly_msgs::ConnectionInfoList& connection_list_msg)
{
//TODO check length
    std::lock_guard<std::mutex> lock_connection_list(connection_list_mutex_);
    ROS_INFO("Received connection message");
    current_connection_list_ = connection_list_msg;
    connection_list_received_ = true;
}

template <typename Tracker>
void ObjectTrackerRos<Tracker>::shutdown()
{
    running_ = false;
}

template <typename Tracker>
void ObjectTrackerRos<Tracker>::run()
{
    running_ = true;

    while (ros::ok() && running_)
    {
        if (!obsrv_updated_)
        {
            usleep(100);
            continue;
        }

        run_once();
    }
}

template <typename Tracker>
bool ObjectTrackerRos<Tracker>::run_once()
{
    if (!obsrv_updated_) return false;

    Obsrv obsrv;
    sensor_msgs::Image ros_image;
    {
        std::lock_guard<std::mutex> lock_obsrv(obsrv_mutex_);
        ros_image      = current_ros_image_;
        obsrv_updated_ = false;
    }

    if (use_input_)
    {
        if (!input_updated_) return false;
        object_assembly_msgs::Input input_msg;
        {
            std::lock_guard<std::mutex> lock_input(input_mutex_);
            input_msg = current_input_;
        }
        if (use_connection_list_)
        {
            object_assembly_msgs::ConnectionInfoList connection_list_msg;
            connection_list_msg.connections.clear();
            if (connection_list_received_)
            {
                std::lock_guard<std::mutex> lock_connection_list(connection_list_mutex_);
                connection_list_msg = current_connection_list_;
            }
            track(ros_image, input_msg, connection_list_msg);
        }
        else
        {
            object_assembly_msgs::ConnectionInfoList connection_list_msg;
            connection_list_msg.connections.clear();
            track(ros_image, input_msg, connection_list_msg);
        }
    }
    else
    {
        if (use_connection_list_)
        {
            object_assembly_msgs::ConnectionInfoList connection_list_msg;
            if (connection_list_received_)
            {
                std::lock_guard<std::mutex> lock_connection_list(connection_list_mutex_);
                connection_list_msg = current_connection_list_;
            }
            track(ros_image, connection_list_msg);
        }
        else
        {
            track(ros_image);
        }

    }
    return true;
}

template <typename Tracker>
auto ObjectTrackerRos<Tracker>::current_state() const -> State
{
    return current_state_;
}

template <typename Tracker>
auto ObjectTrackerRos<Tracker>::current_pose() const
    -> geometry_msgs::PoseStamped
{
    return current_poses_[0];
}


template <typename Tracker>
auto ObjectTrackerRos<Tracker>::current_state_messages() const
    -> std::vector<dbot_ros_msgs::ObjectState>
{
    std::vector<dbot_ros_msgs::ObjectState> state_messages;

    for (int i = 0; i < current_poses_.size(); ++i)
    {
        dbot_ros_msgs::ObjectState object_state_message;
        object_state_message.pose = current_poses_[i];
        object_state_message.velocity = current_velocities_[i];
        state_messages.push_back(object_state_message);
    }

    return state_messages;
}

template <typename Tracker>
auto ObjectTrackerRos<Tracker>::current_poses() const
    -> std::vector<geometry_msgs::PoseStamped>
{
    return current_poses_;
}
}
