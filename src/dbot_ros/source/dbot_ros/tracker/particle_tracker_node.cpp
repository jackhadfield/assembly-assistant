/*
 * This is part of the Bayesian Object Tracking (bot),
 * (https://github.com/bayesian-object-tracking)
 *
 * Copyright (c) 2015 Max Planck Society,
 * 				 Autonomous Motion Department,
 * 			     Institute for Intelligent Systems
 *
 * This Source Code Form is subject to the terms of the GNU General Public
 * License License (GNU GPL). A copy of the license can be found in the LICENSE
 * file distributed with this source code.
 */

/**
 * \file particle_ros_object_tracker.cpp
 * \date November 2015
 * \author Jan Issac (jan.issac@gmail.com)
 */

#include <Eigen/Dense>

#include <fstream>
#include <ctime>
#include <memory>

#include <ros/ros.h>
#include <ros/package.h>

#include <fl/util/profiling.hpp>

#include <opi/interactive_marker_initializer.hpp>
#include <osr/free_floating_rigid_bodies_state.hpp>

#include <dbot/camera_data.hpp>
#include <dbot/simple_wavefront_object_loader.hpp>
#include <dbot/tracker/particle_tracker.hpp>
#include <dbot/builder/particle_tracker_builder.hpp>

#include <dbot_ros/object_tracker_ros.h>
#include <dbot_ros/object_tracker_publisher.h>
#include <dbot_ros/util/ros_interface.hpp>
#include <dbot_ros/util/ros_camera_data_provider.hpp>

#include <object_assembly_msgs/FetchSurfaceParams.h>

void write_values(std::string filepath, int num_objects, dbot::ParticleTracker::State save_values)
{
    std::ofstream myfile;
    myfile.open (filepath);
    for (int i = 0; i < num_objects; i++)
    {
        myfile << save_values.component(i) << "\n";
    }
    myfile.close();
}

void load_initial_poses(std::string filepath, int num_objects, dbot::ParticleTracker::State &initial_poses)
{
    int cols = 0, rows = 0;
    double *buff = new double[12*num_objects];
    // Read numbers from file into buffer.
    std::ifstream infile;
    infile.open(filepath);
    while (! infile.eof())
        {
        std::string line;
        getline(infile, line);

        int temp_cols = 0;
        std::stringstream stream(line);
        while(! stream.eof())
            stream >> buff[cols*rows + temp_cols++];

        if (temp_cols == 0)
            continue;

        if (cols == 0)
            cols = temp_cols;

        rows++;
        }

    infile.close();

    if (rows == 0)
    {
        ROS_ERROR("Initial pose file doesn't exist\n");
        exit(-1);
    }

    // Populate matrix with numbers.
    for (int i = 0; i < num_objects; i++)
        for (int j = 0; j < 12; j++)
            initial_poses.component(i)(j) = buff[12*i+j];

    delete[] buff;
}

void get_initial_poses(const std::string topic, dbot::ParticleTracker::State &initial_poses)
{
    geometry_msgs::PoseArray poses;
    geometry_msgs::PoseArrayConstPtr msg = ros::topic::waitForMessage<geometry_msgs::PoseArray>(topic, ros::Duration());
    if (msg == NULL)
        ROS_INFO("No messages received");
    else
        poses = * msg;
    for (int i = 0; i < poses.poses.size(); i++)
        initial_poses.component(i) = ri::to_pose_velocity_vector(poses.poses[i]);
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "particle_tracker");
    ros::NodeHandle nh("~");

    /* ---------------------------------------------------------------------- */
    /* Roa-Blackwellized Coordinate Particle Filter Object Tracker            */
    /*                                                                        */
    /* Ingredients:                                                           */
    /*   - ObjectTrackerRos                                                   */
    /*     - Tracker                                                          */
    /*       - Rbc Particle Filter Algorithm                                  */
    /*         - Objects state transition model                                */
    /*         - Observation model                                            */
    /*       - Object model                                                   */
    /*       - Camera data                                                    */
    /*     - Tracker publisher to advertise the estimated state               */
    /*                                                                        */
    /*  Construnction of the tracker will utilize few builders and factories. */
    /*  For that, we need the following builders/factories:                   */
    /*    - Object state transition model builder                             */
    /*    - Observation model builder to build GPU or CPU based models        */
    /*    - Filter builder                                                    */
    /* ---------------------------------------------------------------------- */

    // parameter shorthand prefix
    std::string pre = "particle_filter/";


    /* ------------------------------ */
    /* - Create the object model    - */
    /* ------------------------------ */
    // get object parameters
    std::string object_package;
    std::string object_directory;
    std::vector<std::string> object_meshes;

    /// \todo nh.getParam does not check whether the parameter exists in the
    /// config file. this is dangerous, we should use ri::read instead

    nh.getParam("object/meshes", object_meshes);
    nh.getParam("object/package", object_package);
    nh.getParam("object/directory", object_directory);

    // Use the ORI to load the object model usign the
    // SimpleWavefrontObjectLoader
    dbot::ObjectResourceIdentifier ori;
    ori.package_path(ros::package::getPath(object_package));
    ori.directory(object_directory);
    ori.meshes(object_meshes);

    auto object_model_loader = std::shared_ptr<dbot::ObjectModelLoader>(
        new dbot::SimpleWavefrontObjectModelLoader(ori));

    // Load the model usign the simple wavefront load and center the frames
    // of all object part meshes
    bool center_object_frame;
    nh.getParam(pre + "center_object_frame", center_object_frame);
    auto object_model = std::make_shared<dbot::ObjectModel>(
        object_model_loader, center_object_frame);

    /* ------------------------------ */
    /* - Setup camera data          - */
    /* ------------------------------ */
    bool get_resolution_from_topic;
    nh.getParam("get_resolution_from_topic", get_resolution_from_topic);
    int downsampling_factor;
    std::string camera_info_topic;
    std::string depth_image_topic;
    dbot::CameraData::Resolution resolution;
    nh.getParam("camera_info_topic", camera_info_topic);
    nh.getParam("depth_image_topic", depth_image_topic);
    nh.getParam("downsampling_factor", downsampling_factor);
    if (!get_resolution_from_topic)
    {
        nh.getParam("resolution/width", resolution.width);
        nh.getParam("resolution/height", resolution.height);
    }
    else
    {
        sensor_msgs::CameraInfoConstPtr msg;
        msg = ros::topic::waitForMessage<sensor_msgs::CameraInfo>(camera_info_topic, ros::Duration());
        if (msg == NULL)
            ROS_INFO("No messages received");
        else 
        {   std::cout << "Width: " << (*msg).width << ", Height: " << (*msg).height << "\n";
            resolution.width = (*msg).width;
            resolution.height = (*msg).height;
        }
    }

    auto camera_data_provider = std::shared_ptr<dbot::CameraDataProvider>(
        new dbot::RosCameraDataProvider(nh,
                                        camera_info_topic,
                                        depth_image_topic,
                                        resolution,
                                        downsampling_factor,
                                        60.0));
    // Create camera data from the RosCameraDataProvider which takes the data
    // from a ros camera topic
    auto camera_data = std::make_shared<dbot::CameraData>(camera_data_provider);

    /* ------------------------------ */
    /* - Few types we will be using - */
    /* ------------------------------ */
    typedef osr::FreeFloatingRigidBodiesState<> State;
    typedef dbot::ParticleTracker Tracker;
    typedef dbot::ParticleTrackerBuilder<Tracker> TrackerBuilder;
    typedef TrackerBuilder::TransitionBuilder TransitionBuilder;
    typedef TrackerBuilder::SensorBuilder SensorBuilder;


    /* ------------------------------ */
    /* - State transition function  - */
    /* ------------------------------ */
    // We will use a linear observation model built by the object transition
    // model builder. The linear model will generate a random walk.
    dbot::ObjectTransitionBuilder<State>::Parameters params_state;
    // state transition parameters
    nh.getParam(pre + "object_transition/linear_sigma_x",
                params_state.linear_sigma_x);
    nh.getParam(pre + "object_transition/linear_sigma_y",
                params_state.linear_sigma_y);
    nh.getParam(pre + "object_transition/linear_sigma_z",
                params_state.linear_sigma_z);

    nh.getParam(pre + "object_transition/angular_sigma_x",
                params_state.angular_sigma_x);
    nh.getParam(pre + "object_transition/angular_sigma_y",
                params_state.angular_sigma_y);
    nh.getParam(pre + "object_transition/angular_sigma_z",
                params_state.angular_sigma_z);

    nh.getParam(pre + "object_transition/velocity_factor",
                params_state.velocity_factor);
    params_state.part_count = object_meshes.size();

    nh.getParam(pre + "use_input",
                params_state.use_input);
    nh.getParam(pre + "input_weight",
                params_state.input_weight);
    nh.getParam(pre + "input_type",
                params_state.input_type);

    std::string input_topic;
    nh.getParam("input_topic", input_topic);
    std::string connection_list_topic;
    nh.getParam("connection_list_topic", connection_list_topic);

    auto state_trans_builder = std::shared_ptr<TransitionBuilder>(
        new dbot::ObjectTransitionBuilder<State>(params_state));

    /* ------------------------------ */
    /* - Observation model          - */
    /* ------------------------------ */
    dbot::RbSensorBuilder<State>::Parameters params_obsrv;
    nh.getParam(pre + "use_gpu", params_obsrv.use_gpu);

    if (params_obsrv.use_gpu)
    {
        nh.getParam(pre + "gpu/sample_count", params_obsrv.sample_count);
    }
    else
    {
        nh.getParam(pre + "cpu/sample_count", params_obsrv.sample_count);
    }

    nh.getParam(pre + "observation/occlusion/p_occluded_visible",
                params_obsrv.occlusion.p_occluded_visible);
    nh.getParam(pre + "observation/occlusion/p_occluded_occluded",
                params_obsrv.occlusion.p_occluded_occluded);
    nh.getParam(pre + "observation/occlusion/initial_occlusion_prob",
                params_obsrv.occlusion.initial_occlusion_prob);

    nh.getParam(pre + "observation/kinect/tail_weight",
                params_obsrv.kinect.tail_weight);
    nh.getParam(pre + "observation/kinect/model_sigma",
                params_obsrv.kinect.model_sigma);
    nh.getParam(pre + "observation/kinect/sigma_factor",
                params_obsrv.kinect.sigma_factor);
    params_obsrv.delta_time = 1. / 30.;

    // gpu only parameters
    nh.getParam(pre + "gpu/use_custom_shaders",
                params_obsrv.use_custom_shaders);
    nh.getParam(pre + "gpu/vertex_shader_file",
                params_obsrv.vertex_shader_file);
    nh.getParam(pre + "gpu/fragment_shader_file",
                params_obsrv.fragment_shader_file);
    nh.getParam(pre + "gpu/geometry_shader_file",
                params_obsrv.geometry_shader_file);

    nh.getParam(pre + "cpu/thread_count",
                params_obsrv.thread_count);

/* TODO
    std::string surface_params_service;
    nh.getParam("surface_params_service", surface_params_service);
    ros::ServiceClient client =
        nh.serviceClient<object_assembly_msgs::FetchSurfaceParams>
            (surface_params_service);
    object_assembly_msgs::FetchSurfaceParams srv;
    srv.request.recalculate = false;
    while (!client.call(srv))
    {
        ROS_ERROR("Failed to call background removal service. Trying again...");
        usleep(2000000);
    }
    params_obsrv.surface_params[0] =
        srv.response.surface_parameters.a1;
    params_obsrv.surface_params[1] =
        srv.response.surface_parameters.a2;
    params_obsrv.surface_params[2] =
        srv.response.surface_parameters.a3;
*/
    params_obsrv.surface_params[0] = 1;
    params_obsrv.surface_params[1] = 1;
    params_obsrv.surface_params[2] = 1;

    auto sensor_builder =
        std::shared_ptr<SensorBuilder>(new dbot::RbSensorBuilder<State>(
            object_model, camera_data, params_obsrv));

    /* ------------------------------ */
    /* - Create Filter & Tracker    - */
    /* ------------------------------ */
    TrackerBuilder::Parameters params_tracker;
    params_tracker.evaluation_count = params_obsrv.sample_count;
    nh.getParam(pre + "moving_average_update_rate",
                params_tracker.moving_average_update_rate);
    nh.getParam(pre + "max_kl_divergence", params_tracker.max_kl_divergence);
    nh.getParam(pre + "center_object_frame",
                params_tracker.center_object_frame);

    auto tracker_builder = dbot::ParticleTrackerBuilder<Tracker>(
        state_trans_builder, sensor_builder, object_model, params_tracker);
    auto tracker = tracker_builder.build();

    bool use_connection_list;
    nh.getParam(pre + "use_connection_list", use_connection_list);

    float z_input_weight;
    nh.getParam(pre + "z_input_weight", z_input_weight);

    dbot::ObjectTrackerRos<Tracker> ros_object_tracker(
        tracker, camera_data, ori.count_meshes(), params_state.use_input, params_state.input_type, use_connection_list, z_input_weight);

    std::string initial_pose_filepath;
    nh.getParam("initial_pose_filepath", initial_pose_filepath);
    bool save_initial_poses_to_file;
    nh.getParam("save_initial_poses_to_file", save_initial_poses_to_file);
    bool load_initial_poses_from_file;
    nh.getParam("load_initial_poses_from_file", load_initial_poses_from_file);
    bool get_initial_poses_from_topic;
    nh.getParam("get_initial_poses_from_topic", get_initial_poses_from_topic);
    std::string initial_poses_topic;
    nh.getParam("initial_poses_topic", initial_poses_topic);

    std::vector<Tracker::State> initial_poses;
    initial_poses.push_back(Tracker::State(ori.count_meshes()));

    if (!load_initial_poses_from_file)
    {
        if (!get_initial_poses_from_topic)
        {
            /* ------------------------------ */
            /* - Initialize interactively   - */
            /* ------------------------------ */
            opi::InteractiveMarkerInitializer object_initializer(
                camera_data->frame_id(),
                ori.package(),
                ori.directory(),
                ori.meshes(),
                {},
                true);
            if (!object_initializer.wait_for_object_poses())
            {
                ROS_INFO("Setting object poses was interrupted.");
                return 0;
            }

            auto initial_ros_poses = object_initializer.poses();

            int i = 0;
            for (auto& ros_pose : initial_ros_poses)
            {
                initial_poses[0].component(i++) = ri::to_pose_velocity_vector(ros_pose);
            }
        }
        else
            get_initial_poses(initial_poses_topic, initial_poses[0]);
    }
    else
        load_initial_poses(initial_pose_filepath, ori.count_meshes(), initial_poses[0]);
    
    if (save_initial_poses_to_file)
        write_values(initial_pose_filepath, ori.count_meshes(), initial_poses[0]);

    tracker->initialize(initial_poses);

    /* ------------------------------ */
    /* - Tracker publisher          - */
    /* ------------------------------ */
    int object_color[3];
    nh.getParam(pre + "object_color/R", object_color[0]);
    nh.getParam(pre + "object_color/G", object_color[1]);
    nh.getParam(pre + "object_color/B", object_color[2]);

    std::vector<int> r(ori.count_meshes(), 0), g(ori.count_meshes(), 0), b(ori.count_meshes(), 0), object_colors;
    nh.getParam("object_colors", object_colors);
    if (object_colors.size() < 3 * ori.count_meshes())
    {
        ROS_ERROR("Object colors must be a list of 3 X N_objects values (RGB)");
        return -1;
    }
    for (int obj = 0 ; obj < ori.count_meshes(); obj++)
    {
        r[obj] = object_colors[3*obj];
        g[obj] = object_colors[3*obj+1];
        b[obj] = object_colors[3*obj+2];
    }
    auto tracker_publisher = dbot::ObjectStatePublisher(
        ori, r, g, b);

    /* ------------------------------ */
    /* - Run the tracker            - */
    /* ------------------------------ */
    ros::Subscriber subscriber =
        nh.subscribe(depth_image_topic,
                     1,
                     &dbot::ObjectTrackerRos<Tracker>::update_obsrv,
                     &ros_object_tracker);
    (void)subscriber;

    ros::Subscriber input_subscriber, connection_list_subscriber;
    if(params_state.use_input)
    {
        input_subscriber =
            nh.subscribe(input_topic,
                         1,
                         &dbot::ObjectTrackerRos<Tracker>::update_input,
                         &ros_object_tracker);
        (void)input_subscriber;
    }
    if (use_connection_list)
    {
        connection_list_subscriber =
            nh.subscribe(connection_list_topic,
                         1,
                         &dbot::ObjectTrackerRos<Tracker>::update_connection_list,
                         &ros_object_tracker);
        (void)connection_list_subscriber;
    }

    ros::AsyncSpinner spinner(2);
    spinner.start();

    while (ros::ok())
    {
        if (ros_object_tracker.run_once())
        {
            tracker_publisher.publish(
                        ros_object_tracker.current_state_messages());
        }
    }

    return 0;
}
