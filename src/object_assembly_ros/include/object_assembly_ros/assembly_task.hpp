/*
 * This file is part of Assembly Assistant,
 * (https://bitbucket.org/jackhadfield/assembly-assistant)
 *
 * Copyright 2018, Jack Hadfield
 *
 * Assembly Assistant is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Assembly Assistant is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Assembly Assistant.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ASSEMBLY_TASK_HPP_
#define ASSEMBLY_TASK_HPP_

#include <assembly_subtask.hpp>

class AssemblyTask
{

public:
    AssemblyTask(int num_parts,
                 std::vector<std::vector<int>> connnection_pairs,
                 std::vector<AssemblySubtask> subtasks,
                 std::vector<AssemblySubtask> wrong_subtasks,
                 double max_particles,
                 double all_particles,
                 double max_particles_wrong,
                 double all_particles_wrong,
                 std::vector<std::string> descriptions,
                 bool cubic,
                 std::vector<int> identical_objects);
    
    //returns current subtask
    int evaluate_task(std::vector<tf::Transform> current_object_poses, std::vector<object_assembly_msgs::ConnectionInfo> &connection_list, std::vector<bool> &connnection_vector);

    std::string subtask_description(int subtask);

    const int num_subtasks_;
    const int num_wrong_subtasks_;

private:
    const int num_parts_;
    int current_subtask_ = 0;
    std::vector<AssemblySubtask> subtasks_;
    std::vector<AssemblySubtask> wrong_subtasks_;
    double max_particles_;
    double all_particles_;
    double max_particles_wrong_;
    double all_particles_wrong_;
    std::vector<std::string> descriptions_;
    std::vector<std::vector<int>> connection_pairs_;

    int subgraph_max_index_ = -1;
    std::vector<int> part_subgraph_index_; //which subgraph each part belongs to (-1 means none)
    std::vector<tf::Quaternion> rotations_;
    bool cubic_;
};

#endif //ASSEMBLY_TASK_HPP_
