/*
 * This file is part of Assembly Assistant,
 * (https://bitbucket.org/jackhadfield/assembly-assistant)
 *
 * Copyright 2018, Jack Hadfield
 *
 * Assembly Assistant is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Assembly Assistant is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Assembly Assistant.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ASSEMBLY_UI_HPP
#define ASSEMBLY_UI_HPP

#include <vector>
#include <iostream>
#include <string>
#include <cmath>
#include <tf/transform_broadcaster.h>
#include <base_ui.hpp>

class GuessModeUI: public BaseUI
{
public:
    GuessModeUI(std::vector<std::string> filenames, std::string resource_dir);
    void update_probabilities();
    void logic();
    void draw();
    void onDisplay();
    void onIdle();
};

GuessModeUI *ui_global;

#endif //ASSEMBLY_UI_HPP
