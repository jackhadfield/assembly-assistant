/*
 * This file is part of Assembly Assistant,
 * (https://bitbucket.org/jackhadfield/assembly-assistant)
 *
 * Copyright 2018, Jack Hadfield
 *
 * Assembly Assistant is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Assembly Assistant is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Assembly Assistant.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef POSE_INITIALIZER_HPP
#define POSE_INITIALIZER_HPP

#include <stdlib.h>
#include <sstream>
#include <string>
#include <iostream>
#include <vector> 
#include <cmath>
#include <ros/ros.h>
#include <ros/package.h>
#include <Eigen/Dense>
#include "pcl_ros/point_cloud.h"
#include <object_assembly_msgs/Point2D.h>
#include <object_assembly_msgs/Points2D.h>
#include <object_assembly_msgs/Input.h>
#include <object_assembly_msgs/PointCloudArray.h>
#include <object_assembly_msgs/FetchPCLsGivenLocations.h>
#include <object_assembly_msgs/FetchPCLs.h>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <tf/transform_broadcaster.h>
#include <pcl/io/ply_io.h>
#include <pcl/io/pcd_io.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/common/transforms.h>
#include <pcl/registration/icp.h>
#include <Eigen/Geometry> 
#include <geometry_msgs/PoseArray.h>

class PoseInitializerNode {

public:
    PoseInitializerNode(std::vector<std::string> file_paths, int num_objects, std::string background_removal_service, std::string frame_id, int num_random_rotations);

    void colour_tracker_callback(const object_assembly_msgs::Points2D& points);

    void calculate_icp(object_assembly_msgs::PointCloudArray &pcl_array);

    void publish();

    bool init_done_ = false;

private:
    int num_objects_;
    int num_random_rotations_;
    std::string frame_id_;
    std::string background_removal_service_;
    ros::NodeHandle node_handle_;
    ros::Publisher initial_poses_publisher_;
    std::vector<pcl::PointCloud<pcl::PointXYZ>> file_meshes_;
    std::vector<tf::Transform> initial_poses_;

};

#endif //POSE_INITIALIZER_HPP
