/*
 * This file is part of Assembly Assistant,
 * (https://bitbucket.org/jackhadfield/assembly-assistant)
 *
 * Copyright 2018, Jack Hadfield
 *
 * Assembly Assistant is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Assembly Assistant is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Assembly Assistant.  If not, see <http://www.gnu.org/licenses/>.
 */

//#include <Eigen/Dense>

#include <fstream>
#include <ctime>
#include <memory>
#include <thread>

#include <ros/ros.h>
#include <ros/package.h>

#include <assembly_task.hpp>

#include <std_msgs/String.h>
#include <geometry_msgs/PoseArray.h>
#include <dbot_ros_msgs/ObjectsState.h>
#include <object_assembly_msgs/FetchSurfaceParams.h>
#include <object_assembly_msgs/ConnectionInfoList.h>

#include <assembly_ui.hpp>



class AssemblyAgentNode
{

public:

    AssemblyAgentNode(AssemblyTask task, std::string connection_info_list_topic, std::vector<object_assembly_msgs::ConnectionInfo> connection_list, bool use_gui, std::vector<std::string> object_names, int argc, char** argv, std::vector<std::string> mesh_dirs, std::string resource_dir, std::vector<double> colours, std::vector<std::vector<int>> exclusions)
        : task_(task), connection_list_(connection_list), node_handle_("~"), use_gui_(use_gui), object_names_(object_names), argc_(argc), argv_(argv), exclusions_(exclusions)
    {
        ROS_INFO("Creating Assembly Agent Node");

        connection_list_publisher_ = node_handle_.advertise<object_assembly_msgs::ConnectionInfoList>(connection_info_list_topic, 0);
        connection_vector_publisher_ = node_handle_.advertise<object_assembly_msgs::ConnectionVector>("connection_vector", 0);
        connection_vector_string_publisher_ = node_handle_.advertise<std_msgs::String>("connection_vector_string", 0);
        if(use_gui_)
        {
            ui = new GuessModeUI(mesh_dirs, resource_dir);
            for (int i = 0; i < ui->meshes_.size(); i++)
            {
              for (int j = 0; j < ui->meshes_[i].vertices.size(); j++)
                  ui->meshes_[i].set_uniform_colour(glm::vec3(colours[3*i],colours[3*i+1],colours[3*i+2]));
            }
            //This should be ok; a mutex is locked when entering new poses
            std::thread (BaseUI::gui_main_static, argc_, argv_).detach();
        }
    }

    ~AssemblyAgentNode()
    {
        if (use_gui_) delete ui;
    }

    void sort_exclusions()
    { /*
        for (auto ex : exclusions_)
        {
            double max_val = -1.0;
            int max_ind = -1;
            for (auto exx : ex)
                if (connection_list_[exx].num_particles > max_val)
                {
                    max_val = connection_list_[exx].num_particles;
                    max_ind = exx;
                }
            for (auto exx : ex)
                if (exx != max_ind)
                    connection_list_[exx].num_particles = 0;
        }*/

        //for (int i = 0; i < 5; i++)
        //    if (i<3)
        //        ;//connection_list_[i].num_particles = 1.0;
        //    else
        //        connection_list_[i].num_particles = 0;

    }

    /*
    * Publishes a binary vector that describes the assembly's state
    */
    void publish_connection_vector()
    {
        object_assembly_msgs::ConnectionVector connection_vector_msg;
        for (int i = 0; i < connection_vector_.size(); i++)
            connection_vector_msg.v.push_back((char)(connection_vector_[i]));
        connection_vector_publisher_.publish(connection_vector_msg);
    }

    /*
    * Publishes a string containing a binary vector that describes the assembly's state
    */
    void publish_connection_vector_string()
    {
        std_msgs::String connection_vector_string_msg;
        char* data;
        data = (char *) malloc(sizeof(char) * (connection_vector_.size()+1));
        for (int i = 0; i < connection_vector_.size(); i++)
            if (connection_vector_[i])
                data[i] = '1';
            else
                data[i] = '0';
        data[connection_list_.size()] = '\0';
        connection_vector_string_msg.data = std::string(data);
        connection_vector_string_publisher_.publish(connection_vector_string_msg);
    }

    std::vector<tf::Transform> geometry_poses_to_tf_transforms(std::vector<geometry_msgs::Pose>& poses)
    {
        std::vector<tf::Transform> tf_poses;
        for (int i = 0; i < poses.size(); i++)
        {
            tf::Transform single_pose(tf::Quaternion (
                                        poses[i].orientation.x,
                                        poses[i].orientation.y,
                                        poses[i].orientation.z,
                                        poses[i].orientation.w),
                                      tf::Vector3 (
                                        poses[i].position.x,
                                        poses[i].position.y,
                                        poses[i].position.z));
            tf_poses.push_back(single_pose);
        }
        return tf_poses;
    }

    /*
    * Callback for 3D pose state message of type dbot_ros_msgs::ObjectsState
    */
    void objects_state_callback(const dbot_ros_msgs::ObjectsState& state)
    {
        ROS_INFO("Received pose msg");
        std::vector<geometry_msgs::Pose> poses;
        for (int i = 0; i < state.objects_state.size(); i++)
        {        
            poses.push_back (state.objects_state[i].pose.pose);
        }
        current_object_poses_ = geometry_poses_to_tf_transforms(poses);
        int current_subtask;
        if (!task_complete_)
            current_subtask = task_.evaluate_task(current_object_poses_, connection_list_, connection_vector_);
        if (current_subtask == task_.num_subtasks_ || task_complete_)
        {
            std::cout << "You have completed the assembly task!!" << '\n';
            task_complete_ = true;
        }
        else
        {
            std::cout << task_.subtask_description(current_subtask) << " (subtask: " << current_subtask+1 << ")\n";
        }
        sort_exclusions();
        object_assembly_msgs::ConnectionInfoList connections_msg;
        connections_msg.connections = connection_list_;
        connection_list_publisher_.publish(connections_msg);
        publish_connection_vector();
        publish_connection_vector_string();

        if (use_gui_)
        {
            ui->update_poses(current_object_poses_);
        }

        if (current_subtask == task_.num_subtasks_)
        {
           // exit(1); %TODO
        }
    }

    /*
    * Callback for 3D pose state message of type PoseArray
    */
    void pose_array_callback(const geometry_msgs::PoseArray& state)
    {
        ROS_INFO("Received pose msg");
        std::vector<geometry_msgs::Pose> poses = state.poses;
        current_object_poses_ = geometry_poses_to_tf_transforms(poses);
        int current_subtask;
        if (!task_complete_)
            current_subtask = task_.evaluate_task(current_object_poses_, connection_list_, connection_vector_);
        if (current_subtask == task_.num_subtasks_ || task_complete_)
        {
            std::cout << "You have completed the assembly task!!" << '\n';
            task_complete_ = true;
        }
        else
        {
            std::cout << task_.subtask_description(current_subtask) << " (subtask: " << current_subtask+1 << ")\n";
        }
        sort_exclusions();
        object_assembly_msgs::ConnectionInfoList connections_msg;
        connections_msg.connections = connection_list_;
        connection_list_publisher_.publish(connections_msg);
        publish_connection_vector();
        publish_connection_vector_string();

        if (use_gui_)
        {
            ui->update_poses(current_object_poses_);
        }

        if (current_subtask == task_.num_subtasks_)
        {
           // exit(1); %TODO
        }
    }

private:
    AssemblyTask task_;
    ros::NodeHandle node_handle_;
    bool use_gui_;
    std::vector<std::string> object_names_;
    std::vector<tf::Transform> current_object_poses_;
    geometry_msgs::Pose current_relative_pose_;
    std::vector<object_assembly_msgs::ConnectionInfo> connection_list_;
    std::vector<bool> connection_vector_;
    ros::Publisher connection_list_publisher_;
    ros::Publisher connection_vector_publisher_;
    ros::Publisher connection_vector_string_publisher_;
    GuessModeUI *ui;
    int argc_;
    char** argv_;
    bool task_complete_ = false;
    std::vector<std::vector<int>> exclusions_;
};


int main(int argc, char** argv)
{
    ros::init(argc, argv, "assembly_agent");
    ros::NodeHandle nh("~");

    tf::Vector3 up_vector(0,-1,0);
/*
    std::string surface_params_service;
    nh.getParam("surface_params_service", surface_params_service);
    ros::ServiceClient client = nh.serviceClient<object_assembly_msgs::FetchSurfaceParams>(surface_params_service);
    object_assembly_msgs::FetchSurfaceParams srv;
    srv.request.recalculate = true;
    while (!client.call(srv))
    {
        ROS_ERROR("Failed to call background removal service. Trying again...");
        usleep(1000000);
    }
    up_vector.setX(srv.response.surface_parameters.a1);
    up_vector.setY(-1.0);
    up_vector.setZ(srv.response.surface_parameters.a3);
    up_vector.normalize();
*/

    // object data
    std::vector<std::string> object_names;
    std::vector<std::string> object_symmetries;
    std::vector<std::string> identical_objects_str;
    std::vector<int> identical_objects;
    std::string object_pose_topic;
    bool pose_topic_is_dbot_objects_state = true;
    std::vector<std::string> descriptions;
    std::vector<std::string> wrong_descriptions;

    //task data
    int num_subtasks, num_wrong_subtasks, num_checks;
    std::vector<AssemblySubtask> subtasks;
    std::vector<AssemblySubtask> wrong_subtasks;
    bool use_gui;
    std::vector<std::string> mesh_dirs;
    std::string resource_dir;
    std::vector<double> colours;

    /* ------------------------------ */
    /* -     Read out data          - */
    /* ------------------------------ */
    // get object names
    nh.getParam("objects", object_names);
    nh.getParam("object_symmetries", object_symmetries);
    nh.getParam("identical", identical_objects);
    for (int i = 0; i < identical_objects.size(); i++) identical_objects[i]--;
    nh.getParam("object_pose_topic", object_pose_topic);
    nh.getParam("pose_topic_is_dbot_objects_state", pose_topic_is_dbot_objects_state);
    nh.getParam("number_of_checks", num_checks);
    nh.getParam("use_gui", use_gui);
    nh.getParam("resource_dir", resource_dir);
    resource_dir = ros::package::getPath("object_assembly_ros") + "/" + resource_dir;
    if (resource_dir.back() != '/')
        resource_dir.push_back('/');
    nh.getParam("mesh_dirs", mesh_dirs);
    for (int i = 0; i < mesh_dirs.size(); i++)
    {
        mesh_dirs[i] = resource_dir + mesh_dirs[i];
    }
    nh.getParam("colours", colours);
    if (use_gui && colours.size()!=object_names.size()*3)
    {
        ROS_ERROR("Colours parameter is wrong size");
        exit(-1);
    }

    std::vector<object_assembly_msgs::ConnectionInfo> connection_list;
    // get task data
    nh.getParam("number_of_subtasks", num_subtasks);
    nh.param("number_of_wrong_subtasks", num_wrong_subtasks, 0);
    double K;
    nh.getParam("K", K);
    double margin;
    nh.getParam("margin", margin);
    double max_particles;
    nh.getParam("max_particles", max_particles);
    double all_particles;
    nh.getParam("all_particles", all_particles);
    double max_particles_wrong;
    nh.getParam("max_particles_wrong", max_particles_wrong);
    double all_particles_wrong;
    nh.getParam("all_particles_wrong", all_particles_wrong);

    std::vector<std::vector<int>> exclusions;
    for (int i = 1; i <= 3; i++)
    {
        std::vector<int> new_exclusions;
        nh.getParam(std::string("exclusions") + std::to_string(i), new_exclusions);
        exclusions.push_back(new_exclusions);
    }

    std::vector<std::vector<int>> connection_pairs;
    std::vector<std::vector<int>> wrong_connection_pairs;

    for (int i=1; i<=num_subtasks; i++) {
        int first_object;
      	int second_object;

        // subtask shorthand prefix
        std::string subtask_pre;
        subtask_pre = std::string("subtasks/") + "subtask" + std::to_string(i) + "/";

        std::string description;
        nh.getParam(subtask_pre + "description", description);
        descriptions.push_back(description);

        nh.getParam(subtask_pre + "object", second_object);
        nh.getParam(subtask_pre + "relative_object", first_object);
        --first_object;
        --second_object;
        std::vector<int> temp;
        temp.push_back(first_object);
        temp.push_back(second_object);
        connection_pairs.push_back(temp);

        std::vector<double> relative_pose;
        nh.getParam(subtask_pre + "relative_pose", relative_pose);
        tf::Vector3 relative_position;
        relative_position.setValue(relative_pose[0],
                                   relative_pose[1],
                                   relative_pose[2]);
        tf::Quaternion relative_orientation = 
                tf::Quaternion(relative_pose[3],
                               relative_pose[4],
                               relative_pose[5],
                               relative_pose[6]);
        std::string frame;
        nh.getParam(subtask_pre + "frame", frame);
	
        AssemblySubtask subtask(i-1,
                                num_checks,
                                first_object,
                                second_object,
                                frame,
                                relative_position,
                                relative_orientation,
                                margin,
                                object_symmetries,
                                max_particles,
                                up_vector,
                                K,
                                identical_objects);
        subtasks.push_back(subtask);

        //build connection messages to save time later
        object_assembly_msgs::ConnectionInfo connection;
        connection.part = second_object;
        connection.relative_part = first_object;
        connection.relative_pose.position.x = relative_pose[0];
        connection.relative_pose.position.y = relative_pose[1];
        connection.relative_pose.position.z = relative_pose[2];
        connection.relative_pose.orientation.x = relative_pose[3];
        connection.relative_pose.orientation.y = relative_pose[4];
        connection.relative_pose.orientation.z = relative_pose[5];
        connection.relative_pose.orientation.w = relative_pose[6];
        connection.num_particles = 0;
        connection.first_particle = 0;
        connection_list.push_back(connection);
    }

    for (int i=1; i<=num_wrong_subtasks; i++) {
        int first_object;
      	int second_object;

	      // subtask shorthand prefix
        std::string subtask_pre;
        subtask_pre = std::string("wrong_subtasks/") + "subtask" + std::to_string(i) + "/";

        std::string description;
        nh.getParam(subtask_pre + "description", description);
        wrong_descriptions.push_back(description);

        nh.getParam(subtask_pre + "object", second_object);
        nh.getParam(subtask_pre + "relative_object", first_object);
        --first_object;
        --second_object;
        std::vector<int> temp;
        temp.push_back(first_object);
        temp.push_back(second_object);
        wrong_connection_pairs.push_back(temp);

        std::vector<double> relative_pose;
        nh.getParam(subtask_pre + "relative_pose", relative_pose);
        tf::Vector3 relative_position;
        relative_position.setValue(relative_pose[0],
                                   relative_pose[1],
                                   relative_pose[2]);
        tf::Quaternion relative_orientation = 
                tf::Quaternion(relative_pose[3],
                               relative_pose[4],
                               relative_pose[5],
                               relative_pose[6]);
        std::string frame;
        nh.getParam(subtask_pre + "frame", frame);
	
        AssemblySubtask subtask(i-1,
                                num_checks,
                                first_object,
                                second_object,
                                frame,
                                relative_position,
                                relative_orientation,
                                margin,
                                object_symmetries,
                                max_particles,
                                up_vector,
                                K,
                                identical_objects);
        wrong_subtasks.push_back(subtask);

        //build connection messages to save time later
        object_assembly_msgs::ConnectionInfo connection;
        connection.part = second_object;
        connection.relative_part = first_object;
        connection.relative_pose.position.x = relative_pose[0];
        connection.relative_pose.position.y = relative_pose[1];
        connection.relative_pose.position.z = relative_pose[2];
        connection.relative_pose.orientation.x = relative_pose[3];
        connection.relative_pose.orientation.y = relative_pose[4];
        connection.relative_pose.orientation.z = relative_pose[5];
        connection.relative_pose.orientation.w = relative_pose[6];
        connection.num_particles = 0;
        connection.first_particle = 0;
        connection_list.push_back(connection);
    }

    bool cubic = (object_symmetries[0]=="cubic");

    AssemblyTask task(object_names.size(), connection_pairs, subtasks, wrong_subtasks, max_particles, all_particles, max_particles_wrong, all_particles_wrong, descriptions, cubic, identical_objects);

    std::string connection_info_list_topic;
    nh.getParam("connection_info_list_topic",
                connection_info_list_topic);

    AssemblyAgentNode assembly_agent_node(task,
                connection_info_list_topic, connection_list, use_gui, object_names, argc, argv, mesh_dirs, resource_dir, colours, exclusions);

    ros::Subscriber subscriber;
    if (pose_topic_is_dbot_objects_state)
      subscriber = nh.subscribe(
        object_pose_topic, 1, &AssemblyAgentNode::objects_state_callback, &assembly_agent_node);
    else
      subscriber = nh.subscribe(
        object_pose_topic, 1, &AssemblyAgentNode::pose_array_callback, &assembly_agent_node);
    ros::spin();

    return 0;
}
