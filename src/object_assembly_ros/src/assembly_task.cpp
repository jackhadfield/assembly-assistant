/*
 * This file is part of Assembly Assistant,
 * (https://bitbucket.org/jackhadfield/assembly-assistant)
 *
 * Copyright 2018, Jack Hadfield
 *
 * Assembly Assistant is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Assembly Assistant is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Assembly Assistant.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assembly_task.hpp>

AssemblyTask::AssemblyTask(
    int num_parts,
    std::vector<std::vector<int>> connection_pairs,
    std::vector<AssemblySubtask> subtasks,
    std::vector<AssemblySubtask> wrong_subtasks,
    double max_particles,
    double all_particles,
    double max_particles_wrong,
    double all_particles_wrong,
    std::vector<std::string> descriptions,
    bool cubic,
    std::vector<int> identical_objects
    )
    : num_parts_(num_parts),
      connection_pairs_(connection_pairs),
      num_subtasks_(subtasks.size()),
      subtasks_(subtasks),
      num_wrong_subtasks_(wrong_subtasks.size()),
      wrong_subtasks_(wrong_subtasks),
      max_particles_(max_particles),
      all_particles_(all_particles),
      max_particles_wrong_(max_particles_wrong),
      all_particles_wrong_(all_particles_wrong),
      descriptions_(descriptions),
      cubic_(cubic)
{
    tf::Quaternion unit_quaternion(0.0, 0.0, 0.0, 1.0);
    for (int i = 0; i < num_parts_; i++)
    {
        rotations_.push_back(unit_quaternion);
        part_subgraph_index_.push_back(-1);
    }
}

int AssemblyTask::evaluate_task(std::vector<tf::Transform> current_object_poses, std::vector<object_assembly_msgs::ConnectionInfo> &connection_list, std::vector<bool> &connnection_vector)
{
    connnection_vector = std::vector<bool>(num_subtasks_ + num_wrong_subtasks_, false);
    for (int i = 0 ; i < current_subtask_; i++)
        connnection_vector[i] = true;

    if (current_subtask_==num_subtasks_)
    {
        //Assembly finished! Return number of subtasks
        return current_subtask_;
    }
       
    double score;
    //Check wrong connections
    for (int i_wst = 0; i_wst < num_wrong_subtasks_; i_wst++)
    {
        if (wrong_subtasks_[i_wst].evaluate_subtask(current_object_poses, score))
        {
            connection_list[i_wst + num_subtasks_].relative_pose = wrong_subtasks_[i_wst].connection_pose;
            connection_list[i_wst + num_subtasks_].first_particle = 0;
            connection_list[i_wst + num_subtasks_].num_particles = all_particles_wrong_;
            connnection_vector[i_wst + num_subtasks_] = true;
        }
        else
        {
            connection_list[i_wst + num_subtasks_].relative_pose = wrong_subtasks_[i_wst].connection_pose;
            connection_list[i_wst + num_subtasks_].num_particles = int(score * max_particles_wrong_);
        }
    }

    bool switch_first, switch_second;
    //Check right connections
    while (subtasks_[current_subtask_].evaluate_subtask_with_switching(current_object_poses, score, switch_first, switch_second))
    {
        connnection_vector[current_subtask_] = true;
        if (cubic_)
        {
            //Update rotations and subgraph indices
            if (part_subgraph_index_[connection_pairs_[current_subtask_][0]] == -1 &&
                part_subgraph_index_[connection_pairs_[current_subtask_][1]] == -1)
            {
                rotations_[connection_pairs_[current_subtask_][0]] = subtasks_[current_subtask_].first_rotation;
                rotations_[connection_pairs_[current_subtask_][1]] = subtasks_[current_subtask_].first_rotation * subtasks_[current_subtask_].second_rotation;
                rotations_[connection_pairs_[current_subtask_][1]].normalize();
                part_subgraph_index_[connection_pairs_[current_subtask_][0]] = ++subgraph_max_index_;
                part_subgraph_index_[connection_pairs_[current_subtask_][1]] = subgraph_max_index_;
            }
            else if (part_subgraph_index_[connection_pairs_[current_subtask_][1]] == -1)
            {
                rotations_[connection_pairs_[current_subtask_][1]] = rotations_[connection_pairs_[current_subtask_][0]] * subtasks_[current_subtask_].second_rotation;
                part_subgraph_index_[connection_pairs_[current_subtask_][1]] = part_subgraph_index_[connection_pairs_[current_subtask_][0]];;
            }
            else if(part_subgraph_index_[connection_pairs_[current_subtask_][0]] == -1)
            {
                rotations_[connection_pairs_[current_subtask_][0]] = rotations_[connection_pairs_[current_subtask_][1]] * subtasks_[current_subtask_].first_rotation;
                part_subgraph_index_[connection_pairs_[current_subtask_][0]] = part_subgraph_index_[connection_pairs_[current_subtask_][1]];
            }
            else
            {
                int subgraph_ind0 = part_subgraph_index_[connection_pairs_[current_subtask_][0]];
                int subgraph_ind1 = part_subgraph_index_[connection_pairs_[current_subtask_][1]];
                for (int j = 0; j < num_parts_; j++)
                {
                    if (part_subgraph_index_[j] == subgraph_ind1)
                        part_subgraph_index_[j] = subgraph_ind0;
                }
            }
        }

        //Set connection info
        connection_list[current_subtask_].relative_pose = subtasks_[current_subtask_].connection_pose;
        connection_list[current_subtask_].first_particle = 0;
        connection_list[current_subtask_].num_particles = all_particles_;
        if (switch_first)
        {
            connection_list[current_subtask_].relative_part = subtasks_[current_subtask_].first_alternative_;

            for (int s = current_subtask_ + 1; s < num_subtasks_; s++)
            {
                if (subtasks_[s].first_object_ == subtasks_[current_subtask_].first_alternative_ ||
                    subtasks_[s].first_alternative_ == subtasks_[current_subtask_].first_alternative_)
                {
                    subtasks_[s].first_object_ = subtasks_[s].first_alternative_;
                    subtasks_[s].first_alternative_ = -1;
                }
                if (subtasks_[s].second_object_ == subtasks_[current_subtask_].first_alternative_ ||
                    subtasks_[s].second_alternative_ == subtasks_[current_subtask_].first_alternative_)
                {
                    subtasks_[s].second_object_ = subtasks_[s].second_alternative_;
                    subtasks_[s].second_alternative_ = -1;
                }
            }
        }
        else
        {
            for (int s = current_subtask_ + 1; s < num_subtasks_; s++)
            {
                if (subtasks_[s].first_object_ == subtasks_[current_subtask_].first_object_ ||
                    subtasks_[s].first_alternative_ == subtasks_[current_subtask_].first_object_)
                    subtasks_[s].first_alternative_ = -1;
                if (subtasks_[s].second_object_ == subtasks_[current_subtask_].first_object_ ||
                    subtasks_[s].second_alternative_ == subtasks_[current_subtask_].first_object_)
                    subtasks_[s].second_alternative_ = -1;
            }
        }
        if (switch_second)
        {
            connection_list[current_subtask_].part = subtasks_[current_subtask_].second_alternative_;  

            for (int s = current_subtask_ + 1; s < num_subtasks_; s++)
            {
                if (subtasks_[s].first_object_ == subtasks_[current_subtask_].second_alternative_ ||
                    subtasks_[s].first_alternative_ == subtasks_[current_subtask_].second_alternative_)
                {
                    subtasks_[s].first_object_ = subtasks_[s].first_alternative_;
                    subtasks_[s].first_alternative_ = -1;
                }
                if (subtasks_[s].second_object_ == subtasks_[current_subtask_].second_alternative_ ||
                    subtasks_[s].second_alternative_ == subtasks_[current_subtask_].second_alternative_)
                {
                    subtasks_[s].second_object_ = subtasks_[s].second_alternative_;
                    subtasks_[s].second_alternative_ = -1;
                }
            }          
        }
        else
        {
            for (int s = current_subtask_ + 1; s < num_subtasks_; s++)
            {
                if (subtasks_[s].first_object_ == subtasks_[current_subtask_].second_object_ ||
                    subtasks_[s].first_alternative_ == subtasks_[current_subtask_].second_object_)
                    subtasks_[s].first_alternative_ = -1;
                if (subtasks_[s].second_object_ == subtasks_[current_subtask_].second_object_ ||
                    subtasks_[s].second_alternative_ == subtasks_[current_subtask_].second_object_)
                    subtasks_[s].second_alternative_ = -1;
            }
        }

        current_subtask_++;
        if (current_subtask_==num_subtasks_)
        {
            //Assembly finished! Return number of subtasks
            return current_subtask_;
        }

        if (cubic_)
        {
            //Set prerotations and object symmetries
            if (part_subgraph_index_[connection_pairs_[current_subtask_][0]] == -1 &&
                part_subgraph_index_[connection_pairs_[current_subtask_][1]] == -1)
            {
                //neither part is connected to a third part
            }
            else if (part_subgraph_index_[connection_pairs_[current_subtask_][1]] == -1)
            {
                //first part is already connected to a third part
                int subgraph_ind = part_subgraph_index_[connection_pairs_[current_subtask_][0]];
                subtasks_[current_subtask_].set_first_object_symmetry("none");
                tf::Quaternion prerotation = rotations_[connection_pairs_[current_subtask_][0]].inverse();
                subtasks_[current_subtask_].set_prerotation(prerotation);
            }
            else if(part_subgraph_index_[connection_pairs_[current_subtask_][0]] == -1)
            {
                //second part is already connected to a third part
                int subgraph_ind = part_subgraph_index_[connection_pairs_[current_subtask_][1]];
                subtasks_[current_subtask_].set_second_object_symmetry("none");
                tf::Quaternion prerotation = rotations_[connection_pairs_[current_subtask_][1]].inverse();
                subtasks_[current_subtask_].set_prerotation(prerotation);
            }
            else
            {
                //both parts are connected to other components
                //Some clutter will be created, but the possible subgraphs are limited in number, so the effect is negligible
                subtasks_[current_subtask_].set_first_object_symmetry("none");
                subtasks_[current_subtask_].set_second_object_symmetry("none");
                tf::Quaternion prerotation1 = rotations_[connection_pairs_[current_subtask_][0]].inverse();
                tf::Quaternion prerotation2 = rotations_[connection_pairs_[current_subtask_][1]].inverse();
                subtasks_[current_subtask_].set_prerotations(prerotation1, prerotation2);
            }
        }
    }
//connection_list[current_subtask_].first_particle = 0;
    connection_list[current_subtask_].relative_pose = subtasks_[current_subtask_].connection_pose;
    connection_list[current_subtask_].num_particles = double(score * max_particles_);
    return current_subtask_;
}

std::string AssemblyTask::subtask_description(int subtask)
{
    return descriptions_[subtask];
}
