/*
 * This file is part of Assembly Assistant,
 * (https://bitbucket.org/jackhadfield/assembly-assistant)
 *
 * Copyright 2018, Jack Hadfield
 *
 * Assembly Assistant is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Assembly Assistant is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Assembly Assistant.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sstream>
#include <string>
#include <iostream>
#include <fstream>
#include <opencv2/opencv.hpp>
#include <opencv/cv.h>
#include <vector> 
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <ros/ros.h>
#include <ros/package.h>
#include <message_filters/subscriber.h>
#include "selector.h"
#include <std_msgs/Int32MultiArray.h>

using namespace cv;

class ImageCropNode{

public:
    ImageCropNode(int crop_height,
                  int crop_width,
                  int crop_top,
                  int crop_left,
                  double scaling_factor,
                  std::string new_type,
                  bool crop_online,
                  bool use_file,
                  std::string filepath);
    void write_crop_range_to_file();
    bool load_crop_range_from_file();
    void CameraInfoCallback(const sensor_msgs::CameraInfo &cinfo);
    void image_callback(const sensor_msgs::ImageConstPtr& image);
    void colour_callback(const sensor_msgs::ImageConstPtr& image);

private:
    int height_;
    int width_;
    bool image_received_ = false;
    bool cinfo_set_ = false;
    int crop_height_;
    int crop_width_;
    int crop_top_;
    int crop_left_;
    double scaling_factor_;
    std::string new_type_;
    bool initial_state_;
    ros::NodeHandle node_handle_;
    ros::Publisher cropped_image_publisher_;
    ros::Publisher newcamerainfo_publisher_;
    ros::Publisher crop_range_publisher_;

    sensor_msgs::CameraInfo cinfo_;
    Selector* crop_range_selector_;
    const char* windowname = "Crop Image";
    std_msgs::Int32MultiArray crop_range_msg_;
    std::string filepath_;
    bool save_to_file_;
    bool file_exists_;
    bool show_window_;
};

ImageCropNode::ImageCropNode(int crop_height,
                             int crop_width,
                             int crop_top,
                             int crop_left,
                             double scaling_factor,
                             std::string new_type,
                             bool crop_online,
                             bool save_to_file,
                             std::string filepath) : node_handle_("~"),
                                                     crop_height_(crop_height),
                                                     crop_width_(crop_width),
                                                     crop_top_(crop_top),
                                                     crop_left_(crop_left),
                                                     scaling_factor_(scaling_factor),
                                                     new_type_(new_type),
                                                     initial_state_(crop_online),
                                                     show_window_(crop_online),
                                                     save_to_file_(save_to_file),
                                                     filepath_(filepath)
{
    ROS_INFO("Creating Image Crop Node");
    cropped_image_publisher_ = node_handle_.advertise<sensor_msgs::Image>("cropped_image", 0);
    newcamerainfo_publisher_ = node_handle_.advertise<sensor_msgs::CameraInfo>("newCameraInfo", 0);
    crop_range_publisher_ = node_handle_.advertise<std_msgs::Int32MultiArray>("crop_range", 0);

    if (!crop_online)
    {
        crop_range_msg_.data.push_back(crop_left_);
        crop_range_msg_.data.push_back(crop_top_);
        crop_range_msg_.data.push_back(crop_width_);
        crop_range_msg_.data.push_back(crop_height_);
        if (save_to_file_) write_crop_range_to_file();
    }
    else        
    {
        namedWindow(windowname, WINDOW_AUTOSIZE);
        crop_range_selector_ = new Selector(windowname);
        cvStartWindowThread();
    }
}

void ImageCropNode::write_crop_range_to_file()
{
    std::ofstream myfile;
    myfile.open (filepath_);
    myfile << crop_left_ << "\n";
    myfile << crop_top_ << "\n";
    myfile << crop_width_ << "\n";
    myfile << crop_height_ << "\n";
    myfile.close();
}

bool ImageCropNode::load_crop_range_from_file()
{
    int cols = 0, rows = 0;
    int *buff = new int[4];
    // Read numbers from file into buffer.
    std::ifstream infile;
    infile.open(filepath_);
    if (infile.fail())
    {
        ROS_INFO("Crop range file doesn't exist\n");
        delete[] buff;
        return false;
    }

    while (! infile.eof())
    {
        std::string line;
        getline(infile, line);

        int temp_cols = 0;
        std::stringstream stream(line);
        while(! stream.eof())
            stream >> buff[cols*rows + temp_cols++];

        if (temp_cols == 0)
            continue;

        if (cols == 0)
            cols = temp_cols;

        rows++;
    }

    infile.close();

    if (rows < 4)
    {
        ROS_INFO("Crop range file doesn't contain enough values.\n");
        delete[] buff;
        return false;
    }

    crop_left_ = buff[0];
    crop_top_ = buff[1];
    crop_width_ = buff[2];
    crop_height_ = buff[3];
    delete[] buff;

    return true;
}

void ImageCropNode::CameraInfoCallback(const sensor_msgs::CameraInfo &cinfo) {
    if (image_received_) {
        if (!cinfo_set_) {
            cinfo_ = cinfo;
            cinfo_.P[2] = width_/2 - crop_left_ - 0.5;
            cinfo_.P[6] = height_/2 - crop_top_ - 0.5;
            cinfo_.K[2] = cinfo_.P[2];
            cinfo_.K[5] = cinfo_.P[6];
            cinfo_.height = crop_height_;
            cinfo_.width = crop_width_;
            cinfo_set_ = true;
        }
        newcamerainfo_publisher_.publish(cinfo_);
    }
}

void ImageCropNode::image_callback(
        const sensor_msgs::ImageConstPtr& image) {
    cv_bridge::CvImagePtr cv_ptr;
    try {
        cv_ptr = cv_bridge::toCvCopy(image);
    }
    catch (cv_bridge::Exception& e) {
        ROS_ERROR("Could not convert from '%s' to 'mono16'.",
                  image->encoding.c_str());
    }

    if (initial_state_)
    {
        putText(cv_ptr->image, "Press c to select crop range", Point(20, 20), 1, 1, Scalar(0xffff), 2);
        putText(cv_ptr->image, "Press n for no cropping", Point(20, 50), 1, 1, Scalar(0xffff), 2);
        putText(cv_ptr->image, "Press f use range in config file", Point(20, 80), 1, 1, Scalar(0xffff), 2);
        putText(cv_ptr->image, "Press l to load range from temporary file", Point(20, 110), 1, 1, Scalar(0xffff), 2);
        imshow(windowname, cv_ptr->image);
        char c;
        c = waitKey(30);
        switch (c)
        {
            case 'c': case 'C':
                while (!crop_range_selector_->valid())
                {
                    Mat temp;
                    cv_ptr->image.copyTo(temp);
                    if (crop_range_selector_->m_selecting)
                        rectangle(temp, crop_range_selector_->m_selection, Scalar(0xffff), 1);
                    imshow(windowname, temp);
                    waitKey(30);
                }
                crop_left_ = crop_range_selector_->selection().x;
                crop_top_ = crop_range_selector_->selection().y;
                crop_width_ = crop_range_selector_->selection().width;
                crop_height_ = crop_range_selector_->selection().height;
                initial_state_ = false;
                if (save_to_file_) write_crop_range_to_file();
                crop_range_msg_.data.push_back(crop_left_);
                crop_range_msg_.data.push_back(crop_top_);
                crop_range_msg_.data.push_back(crop_width_);
                crop_range_msg_.data.push_back(crop_height_);
                break;
            case 'n': case 'N':
                crop_left_ = 0;
                crop_top_ = 0;
                crop_width_ = cv_ptr->image.cols;
                crop_height_ = cv_ptr->image.rows;
            case 'f': case 'F':
                crop_range_msg_.data.push_back(crop_left_);
                crop_range_msg_.data.push_back(crop_top_);
                crop_range_msg_.data.push_back(crop_width_);
                crop_range_msg_.data.push_back(crop_height_);
                if (save_to_file_) write_crop_range_to_file();
                initial_state_ = false;
                break;
            case 'l': case 'L':
                file_exists_ = load_crop_range_from_file();
                crop_range_msg_.data.push_back(crop_left_);
                crop_range_msg_.data.push_back(crop_top_);
                crop_range_msg_.data.push_back(crop_width_);
                crop_range_msg_.data.push_back(crop_height_);
                if (file_exists_) initial_state_ = false;
                break;
            default:
                ;
        }
    }
    else
    {
        image_received_ = true;
        height_ = cv_ptr->image.rows;
        width_ = cv_ptr->image.cols;
        if (crop_left_+crop_width_ > width_ || crop_top_+crop_height_ > height_)
        {
            ROS_ERROR("Image crop: Invalid crop parameters");
            return;
        }
        Rect r1(crop_left_, crop_top_, crop_width_, crop_height_);
        Mat img = cv_ptr->image(r1);
        if (show_window_)
        {           
            Mat temp;
            img.copyTo(temp);
            putText(temp, "Press e to close window", Point(20, 20), 1, 1, Scalar(0xffff), 2);
            imshow(windowname, temp);
            char c;
            c = waitKey(30);
            switch (c)
            {
                case 'e': case 'E':
                    show_window_ = false;
                    cvDestroyWindow(windowname);
                default:
                    ;
            }
        }
        cv_ptr->image = img;
        if (new_type_ == "32FC1")
            cv_ptr->image.convertTo(cv_ptr->image, CV_32FC1, scaling_factor_);
        else
            cv_ptr->image.convertTo(cv_ptr->image, CV_16UC1, scaling_factor_);
        auto img_msg = cv_ptr->toImageMsg();
        img_msg->encoding = new_type_;
        cropped_image_publisher_.publish(img_msg);
        crop_range_publisher_.publish(crop_range_msg_);
    }
}


int main(int argc, char* argv[]) {
    ros::init(argc, argv, "image_crop");
    ros::NodeHandle nh("~");

    std::string image_topic;
    nh.getParam("image_topic", image_topic);
    std::string camera_info_topic;
    nh.getParam("camera_info_topic", camera_info_topic);

    std::string new_type;;
    nh.getParam("convert_to", new_type);

    int crop_height=-1, crop_width=-1, crop_top=-1, crop_left=-1;
    double scaling_factor;
    bool crop_online;
    nh.getParam("crop_online", crop_online);
    nh.getParam("crop_height", crop_height);
    nh.getParam("crop_width", crop_width);
    nh.getParam("crop_top", crop_top);
    nh.getParam("crop_left", crop_left);
    nh.getParam("scaling_factor", scaling_factor);

    if (crop_left < 0 || crop_width < 0 || crop_top < 0 || crop_height < 0 || scaling_factor < 0)
    {
        ROS_ERROR("Negative crop parameters");
        return -1;
    }

    bool save_to_file;
    nh.getParam("save_to_file", save_to_file);
    std::string filepath;
    nh.getParam("file_path", filepath);

    ImageCropNode image_crop_node(crop_height, crop_width, crop_top, crop_left, scaling_factor, new_type, crop_online, save_to_file, filepath);

    ros::Subscriber image_subscriber = 
        nh.subscribe(image_topic,
                     1,
                     &ImageCropNode::image_callback,
                     &image_crop_node);

    ros::Subscriber cinfosubscriber =
        nh.subscribe(camera_info_topic,
                     1,
                     &ImageCropNode::CameraInfoCallback,
                     &image_crop_node);

    ros::spin();
}
