/*
 * This file is part of Assembly Assistant,
 * (https://bitbucket.org/jackhadfield/assembly-assistant)
 *
 * Copyright 2018, Jack Hadfield
 *
 * Assembly Assistant is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Assembly Assistant is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Assembly Assistant.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sstream>
#include <string>
#include <iostream>
#include <fstream>
#include <vector> 
#include <ros/ros.h>
#include <ros/package.h>
#include <message_filters/subscriber.h>
#include <std_msgs/Int8.h>

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "online_annotation");
    ros::NodeHandle nh("~");

    ros::Publisher publisher = nh.advertise<std_msgs::Int8>("annotation", 0);
    int buffer;
    std_msgs::Int8 msg;
    ros::Rate r(30); //30Hz
    while (ros::ok())
    {
        std::cin >> buffer;
        msg.data = buffer;
        publisher.publish(msg);
        r.sleep();
    }
}
