/*
 * This file is part of Assembly Assistant,
 * (https://bitbucket.org/jackhadfield/assembly-assistant)
 *
 * Copyright 2018, Jack Hadfield
 *
 * Assembly Assistant is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Assembly Assistant is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Assembly Assistant.  If not, see <http://www.gnu.org/licenses/>.
 */



#include <stdlib.h>
#include <sstream>
#include <string>
#include <iostream>
#include <vector> 
#include <cmath>
#include <ros/ros.h>
#include <ros/package.h>
#include <dbot_ros_msgs/ObjectsState.h>
#include "points02b.h"
#include <tf/transform_broadcaster.h>

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "test01_publisher");
    ros::NodeHandle nh("~");
    ros::Publisher pub = nh.advertise<dbot_ros_msgs::ObjectsState>("/particle_tracker/objects_state", 0);

    double quaternion_vals[] = 
        {0,0,0,1,
        1,0,0,0,
        0,1,0,0,
        0,0,1,0,
        0.7071,0.7071,0,0, //4
        0.7071,0,0.7071,0,
        0.7071,0,0,0.7071,
        0,0.7071,0.7071,0,
        0,0.7071,0,0.7071, //8
        0,0,0.7071,0.7071,
        -0.7071,0.7071,0,0,
        -0.7071,0,0.7071,0,
        -0.7071,0,0,0.7071, //12
        0,-0.7071,0.7071,0,
        0,-0.7071,0,0.7071,
        0,0,-0.7071,0.7071,
        0.5,0.5,0.5,0.5, //16
        -0.5,0.5,0.5,0.5,
        0.5,-0.5,0.5,0.5,
        0.5,0.5,-0.5,0.5,
        -0.5,-0.5,-0.5,0.5, //20
        -0.5,-0.5,0.5,0.5,
        -0.5,0.5,-0.5,0.5,
        0.5,-0.5,-0.5,0.5};
    int rand_ind[12];
    std::srand(time(NULL));
//std::vector<int> v;
    for (int i=0;i<12;i++){
        rand_ind[i] = rand() % 24 ;
        std::cout << i << ": " << rand_ind[i] << "\n";
        //v.push_back(rand_ind[i]);
    }
    //std::nth_element(v.begin(), v.begin()+4, v.end(), std::greater<int>());
    //std::cout << "Kth: " << v[4] << "\n";
/*
    rand_ind[0] = 8;
    rand_ind[1] = 17;
    rand_ind[2] = 9;
    rand_ind[3] = 5;
    rand_ind[4] = 21;
    rand_ind[5] = 23;
    rand_ind[6] = 11;
    rand_ind[7] = 13;
    rand_ind[8] = 10;
    rand_ind[9] = 9;
    rand_ind[10] = 13;
    rand_ind[11] = 8;
*/
/*
    rand_ind[0] = 1;
    rand_ind[1] = 19;
    rand_ind[2] = 2;
    rand_ind[3] = 13;
    rand_ind[4] = 20;
    rand_ind[5] = 9;
    rand_ind[6] = 22;
    rand_ind[7] = 18;
    rand_ind[8] = 4;
    rand_ind[9] = 21;
    rand_ind[10] = 9;
    rand_ind[11] = 12;
*/
/*
    rand_ind[0] = 23;
    rand_ind[1] = 17;
    rand_ind[2] = 20;
    rand_ind[3] = 12;
    rand_ind[4] = 3;
    rand_ind[5] = 10;
    rand_ind[6] = 4;
    rand_ind[7] = 4;
    rand_ind[8] = 4;
    rand_ind[9] = 9;
    rand_ind[10] = 10;
    rand_ind[11] = 4;
*/
/*
    rand_ind[0] = 8;
    rand_ind[1] = 18;
    rand_ind[2] = 16;
    rand_ind[3] = 21;
    rand_ind[4] = 2;
    rand_ind[5] = 0;
    rand_ind[6] = 13;
    rand_ind[7] = 18;
    rand_ind[8] = 22;
    rand_ind[9] = 20;
    rand_ind[10] = 20;
    rand_ind[11] = 19;
*/
    rand_ind[0] = 11;
    rand_ind[1] = 22;
    rand_ind[2] = 10;
    rand_ind[3] = 17;
    rand_ind[4] = 1;
    rand_ind[5] = 23;
    rand_ind[6] = 9;
    rand_ind[7] = 12;
    rand_ind[8] = 11;
    rand_ind[9] = 8;
    rand_ind[10] = 19;
    rand_ind[11] = 15;


    int ind=0;
    for (int t=0;t<1200;t++)
    {
        dbot_ros_msgs::ObjectsState objects_state_message;
        for (int i = 0; i < 12; i++)
        {
            dbot_ros_msgs::ObjectState temp_object_state_message;
            geometry_msgs::PoseStamped pose_stamped;
            //geometry_msgs::Pose pose;
            pose_stamped.pose.position.x = points[ind++];
            pose_stamped.pose.position.y = points[ind++];
            pose_stamped.pose.position.z = points[ind++];
            if (0)//(i==1||i==2||i==7||i==11)
            {
                pose_stamped.pose.orientation.x = atof(argv[2]);
                pose_stamped.pose.orientation.y = atof(argv[3]);
                pose_stamped.pose.orientation.z = atof(argv[4]);
                pose_stamped.pose.orientation.w = atof(argv[5]);
            }
            else
            {
                pose_stamped.pose.orientation.x = quaternion_vals[4*rand_ind[i]];
                pose_stamped.pose.orientation.y = quaternion_vals[4*rand_ind[i]+1];
                pose_stamped.pose.orientation.z = quaternion_vals[4*rand_ind[i]+2];
                pose_stamped.pose.orientation.w = quaternion_vals[4*rand_ind[i]+3];
            }
            pose_stamped.header.stamp = ros::Time::now();
            pose_stamped.header.frame_id = "no_frame";

            temp_object_state_message.pose = pose_stamped;

            //temp_object_state_message.pose = pose[i];
            temp_object_state_message.ori.name = "no_name.obj";
            temp_object_state_message.ori.directory = "no_directory";
            temp_object_state_message.ori.package = "no_package";

            temp_object_state_message.name = "no_name";

            objects_state_message.objects_state.push_back (temp_object_state_message);
        }
        if (t%10==0) std::cout << "Time: " << t << "\n";
        objects_state_message.active_object_id = 0;
        pub.publish(objects_state_message);
        usleep(atoi(argv[1]));
        ros::spinOnce();
    }

    //ros::spin();
    return 0;
}

