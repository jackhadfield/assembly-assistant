import sys
import time
import math
import numpy
import motion
from naoqi import ALProxy

class NaoGestures():
    def __init__(self):
        # Set Nao's IP and port
        ipAdd = "192.168.0.121"
        port = 9559

        # Set motionProxy
        try:
            self.motionProxy = ALProxy("ALMotion", ipAdd, port)
        except Exception as e:
            print "Could not create proxy to ALMotion"
            print "Error was: ", e
            sys.exit()

        # Set postureProxy
        try:
            self.postureProxy = ALProxy("ALRobotPosture", ipAdd, port)
        except Exception, e:
            print "Could not create proxy to ALRobotPosture"
            print "Error was: ", e
            sys.exit()

        # Set ttsProxy
        try:
            self.ttsProxy = ALProxy("ALTextToSpeech", ipAdd, port)
        except Exception, e:
            print "Could not create proxy to ALTextToSpeech"
            print "Error was: ", e
            sys.exit()

        # Set constants
        self.torsoHeadOffset = numpy.array([0.0, 0.0, 0.1264999955892563])
        self.torsoLShoulderOffset = numpy.array([0.0, 0.09000000357627869, 0.10599999874830246])
        self.torsoRShoulderOffset = numpy.array([0.0, -0.09000000357627869, 0.10599999874830246])
        self.lArmInitPos = [0.11841137707233429, 0.13498550653457642, -0.04563630372285843, -1.2062638998031616, 0.4280231297016144, 0.03072221577167511]
        self.rArmInitPos = [0.11877211928367615, -0.13329118490219116, -0.04420270770788193, 1.2169694900512695, 0.4153063893318176, -0.012792877852916718]
        self.armLength = 0.22 # in meters, rounded down
        self.frame = motion.FRAME_TORSO
        self.axisMask = 7 # just control position
        self.useSensorValues = False
        self.initLArmSet = False
        self.initRArmSet = False
        #StiffnessOn(motionProxy)

        self.returnToInit()

    def StiffnessOn(proxy):
        # We use the "Body" name to signify the collection of all joints
        pNames = "Body"
        pStiffnessLists = 1.0
        pTimeLists = 1.0
        proxy.stiffnessInterpolation(pNames, pStiffnessLists, pTimeLists)

    def say(self, sentence):
        self.ttsProxy.say(sentence)

    def look(self, torsoObjectVector):
        pitch, yaw = self.getPitchAndYaw(torsoObjectVector)
        sleepTime = 0 # seconds
        self.moveHead(pitch, yaw, sleepTime) # Move head to look
        self.moveHead(0, 0, sleepTime) # Move head back

    def point(self, torsoObjectVector):
        pointingArm = "LArm" if torsoObjectVector[1] >= 0 else "RArm"
        shoulderOffset, initArmPosition = self.setArmVars(pointingArm)
        IKTarget = self.getIKTarget(torsoObjectVector, shoulderOffset)
        sleepTime = 0 # seconds
        self.moveArm(pointingArm, IKTarget, sleepTime) # Move arm to point
        self.moveArm(pointingArm, initArmPosition, sleepTime) # Move arm back

    def lookAndPointThenReturn(self, torsoObjectVector):
        pointingArm = "LArm" if torsoObjectVector[1] >= 0 else "RArm"
        pitch, yaw = self.getPitchAndYaw(torsoObjectVector)
        shoulderOffset, initArmPosition = self.setArmVars(pointingArm)
        IKTarget = self.getIKTarget(torsoObjectVector, shoulderOffset)
        sleepTime = 0 # set individual sleep times to 0

        # Move arm and head to gesture
        self.moveArm(pointingArm, IKTarget, sleepTime)
        self.moveHead(pitch, yaw, sleepTime)
        time.sleep(3)
        # Move arm and head back
        self.moveArm(pointingArm, initArmPosition, sleepTime)
        self.moveHead(0, 0, sleepTime)
        time.sleep(0.2)

    def lookAndPoint(self, torsoObjectVector):
        pointingArm = "LArm" if torsoObjectVector[1] >= 0 else "RArm"
        pitch, yaw = self.getPitchAndYaw(torsoObjectVector)
        shoulderOffset, initArmPosition = self.setArmVars(pointingArm)
        IKTarget = self.getIKTarget(torsoObjectVector, shoulderOffset)
        sleepTime = 0 # set individual sleep times to 0

        # Move arm and head to gesture
        self.moveHead(pitch, yaw, sleepTime)
        self.moveArm(pointingArm, IKTarget, sleepTime)
        time.sleep(0.2)

    def returnToInit(self):
        self.postureProxy.goToPosture("StandInit", 0.5)

    def getPitchAndYaw(self, torsoObjectVector):
        # Get unit vector from head to object
        headObjectVector = torsoObjectVector - self.torsoHeadOffset
        headObjectUnitVector = [x / self.magn(headObjectVector) for x in headObjectVector]

        # Compute pitch and yaw of unit vector
        pitch = -math.asin(headObjectUnitVector[2])
        yaw = math.asin(headObjectUnitVector[1])
        return pitch, yaw

    def magn(self, v):
        return math.sqrt(v[0]**2 + v[1]**2 + v[2]**2)

    def moveHead(self, pitch, yaw, sleepTime):
        head = ["HeadPitch", "HeadYaw"]
        fractionMaxSpeed = 0.2
        self.motionProxy.setAngles(head, [pitch, yaw], fractionMaxSpeed)
        time.sleep(sleepTime)

    def setArmVars(self, pointingArm):
        shoulderOffset = None
        initArmPosition = None
        if pointingArm == "LArm":
            shoulderOffset = self.torsoLShoulderOffset
            initArmPosition = self.lArmInitPos
        elif pointingArm == "RArm":
            shoulderOffset = self.torsoRShoulderOffset
            initArmPosition = self.rArmInitPos
        else:
            print "ERROR: Must provide point() with LArm or RArm"
            sys.exit(1)
        return shoulderOffset, initArmPosition

    def getIKTarget(self, torsoObjectVector, shoulderOffset):
        # vector from shoulder to object
        shoulderObjectVector = torsoObjectVector - shoulderOffset

        # scale vector by arm length
        shoulderObjectVectorMagn = self.magn(shoulderObjectVector)
        ratio = self.armLength / shoulderObjectVectorMagn
        IKTarget = [x*ratio for x in shoulderObjectVector]

        # get scaled vector in torso coordinate frame
        IKTarget += shoulderOffset
        IKTarget = list(numpy.append(IKTarget, [0.0, 0.0, 0.0]))
        return IKTarget

    def moveArm(self, pointingArm, IKTarget, sleepTime):
        fractionMaxSpeed = 0.9
        #Open hands
        if pointingArm == "LArm":
            self.motionProxy.openHand('LHand')
        else:
            self.motionProxy.openHand('RHand')
        self.motionProxy.setPosition(pointingArm, self.frame, IKTarget, fractionMaxSpeed, self.axisMask)
        time.sleep(sleepTime)


if __name__ == '__main__':
    # Test code
    torsoObjectVector = [0.4, 0.2, -0.3]
    naoGestures = NaoGestures()
    naoGestures.returnToInit()
    naoGestures.lookAndPoint(torsoObjectVector)
    torsoObjectVector = [0.4, -0.2, -0.3]
    naoGestures.say("Can you put this")
    naoGestures.lookAndPoint(torsoObjectVector)
    naoGestures.say("over here?")
    naoGestures.returnToInit()
