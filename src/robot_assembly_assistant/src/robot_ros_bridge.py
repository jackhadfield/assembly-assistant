#!/usr/bin/env python

import rospy
import socket
import sys
import numpy as np
import ast
import json
from naoGestures import NaoGestures
from zenoGestures import ZenoGestures
from std_msgs.msg import String
from geometry_msgs.msg import PoseArray
from dbot_ros_msgs.msg import ObjectsState
from object_assembly_msgs.msg import ConnectionInfoList
from task1 import Task1
from task2 import Task2
from task3 import Task3

class Robot_ROS_Bridge(object):
    def __init__(self, ros_node_name):
        super(Robot_ROS_Bridge, self).__init__()
        self.ros_node_name = ros_node_name
        self.poses_received = False
        self.poses = []
        #Parameters for coordinate conversion TODO: Change for Zeno
        theta = 2.1780 #angle from ceiling to kinect's looking direction
        d1 = 0.48 #robot distance from kinect along kinect's x-axis
        d2 = 0.70 #robot distance from kinect along axis parallel to tabletop and away from kinect
        hdiff = -0.17 #h_robot-h_camera
        d = np.array([d2, hdiff])
        r = np.array([(np.cos(theta), -np.sin(theta)), (np.sin(theta), np.cos(theta))])
        yz = np.dot(r,d.transpose())
        self.camera2Robot = np.linalg.inv(np.array([(1, 0, 0, -d1), (0, np.cos(theta), -np.sin(theta), yz[0]), (0, np.sin(theta), np.cos(theta), yz[1]), (0,0,0,1)]))
        #Go to main loop
        self.init_ros_and_wait_for_messages()

    def connection_list_callback(self, data):
        '''
        Callback for connection info list
        '''
        self.task.latest_conn_list_msg = data
        self.task.new_conn_list_msg = True

    def connection_vector_callback(self, data):
        '''
        Callback for binary vector of connection states (in string format)
        '''
        self.task.latest_conn_vec_msg = data.data
        self.task.new_conn_vec_msg = True

    def objects_state_callback(self, data):
        '''
        Callback for poses of type dbot_ros_msgs.ObjectsState
        '''
        self.poses = []
        for i in data.objects_state:
            self.poses.append([i.pose.pose.position.x, i.pose.pose.position.y, i.pose.pose.position.z])
        self.poses_received = True

    def pose_array_callback(self, data):
        '''
        Callback for poses of type geometry_msgs.PoseArray
        '''
        self.poses = []
        for i in data.poses:
            self.poses.append([i.position.x, i.position.y, i.position.z])
        self.poses_received = True

    def convertCoords(self, cameraCoords):
        '''
        Converts camera coordinates to Nao coordinates. 
        '''
        p = np.dot(self.camera2Robot, np.array([cameraCoords[0], cameraCoords[1], cameraCoords[2], 1]))
        return [p[0], p[1], p[2]]


    def init_ros_and_wait_for_messages(self):
        rospy.init_node(self.ros_node_name, anonymous=True)
        pub = rospy.Publisher("robot_response", String, queue_size=10)
        task_param = rospy.get_param('~task')
        if task_param == 1:
            self.task = Task1()
        elif task_param == 2:
            self.task = Task2()
        elif task_param == 3:
            self.task = Task3()
        else:
            print("Task ", task_param, " not defined")
            sys.exit()
        self.conn_list_sub = rospy.Subscriber("assembly_agent/connection_list", ConnectionInfoList, self.connection_list_callback)
        self.conn_vec_sub = rospy.Subscriber("assembly_agent/connection_vector_string", String, self.connection_vector_callback)
        self.poses_sub = rospy.Subscriber('/particle_tracker/objects_state', ObjectsState, self.objects_state_callback)
        robot_param = rospy.get_param('~robot')
        if robot_param == "Nao":
            self.robotGestures = NaoGestures()
        elif robot_param == "Zeno":
            self.robotGestures = ZenoGestures()
        else:
            print("Robot gestures for ", robot_param, " not defined")
            sys.exit()
        print "Done setting up robot"
        rate = rospy.Rate(10) # 10 Hz
        while not rospy.is_shutdown():
            self.task.run()
            action, action_data = self.task.get_action()
            if action == "say":
                pub.publish("Say "+action_data)
                self.robotGestures.say(action_data)
            elif action == "point":
                pub.publish("Point "+str(action_data))
                self.robotGestures.lookAndPoint(self.convertCoords(self.poses[action_data]))
            elif action == "return":
                pub.publish("Return")
                self.robotGestures.returnToInit()
            


if __name__ == '__main__':
    ##### customizable parameters #####
    ros_node_name = 'robot_ros_bridge'
    ###################################

bridge = Robot_ROS_Bridge(ros_node_name)
