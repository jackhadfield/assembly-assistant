#!/usr/bin/env python

import rospy
import sys
import numpy as np
import ast
import random
import time
from std_msgs.msg import String
from object_assembly_msgs.msg import ConnectionInfoList
    
# Smaller rectangle using red and green bricks
class Task3():
    def __init__(self):
        self.max_counts = 10 # How many state entries before the messages are repeated
        self.count = self.max_counts
        self.wait_time = 0.5 # How long should the robot pause on state entry (in sec)
        self.wrong_said = False # Whether a correction has already been given recently
        self.new_conn_vec_msg = False
        self.action_queue = [] # Queue of actions to be handled by the robot-ROS bridge
        self.current_state = 0
        self.state_entry = True #Whether the current state is to be reentered

    def random_congratulatory_msg(self):
        congratulatory_msg_list = ["Well done!", "Good job!", "That looks right!", "That looks good!", "Nice!"]
        return random.choice(congratulatory_msg_list)

    # All states must have an entry section. Optionally, they may have an event section, 
    # that is entered when a new connection message is received.
    def state_start(self):
        if self.state_entry:
            self.action_queue.insert(0, ["say", "Hello again! Your last job is to make a square using all the bricks."])
            self.current_state = 1

    def state_first_connection(self):
        if self.state_entry:
            time.sleep(self.wait_time)
            self.state_entry = False
            self.action_queue.insert(0, ["point", self.latest_conn_list_msg.connections[0].relative_part])
            self.action_queue.insert(0, ["say", "Try putting this green brick"])
            self.action_queue.insert(0, ["point", self.latest_conn_list_msg.connections[0].part])
            self.action_queue.insert(0, ["say", "into that blue brick there."])
            self.action_queue.insert(0, ["return", 0])
            self.count = 0
        elif self.new_conn_vec_msg:
            self.new_conn_vec_msg = False
            self.count += 1
            if bool(int(self.latest_conn_vec_msg[0])):
                self.count = self.max_counts
                self.action_queue.insert(0, ["say", self.random_congratulatory_msg()])
                self.state_entry = True
                if not bool(int(self.latest_conn_vec_msg[1])):
                    self.current_state = 2
                elif not bool(int(self.latest_conn_vec_msg[2])):
                    self.current_state = 3
                elif not bool(int(self.latest_conn_vec_msg[3])):
                    self.current_state = 4
                elif not bool(int(self.latest_conn_vec_msg[4])):
                    self.current_state = 5
                else:
                    self.current_state = 6
            elif bool(int(self.latest_conn_vec_msg[5])):
                if not self.wrong_said:
                    self.action_queue.insert(0, ["say", "That's another way to make square, but I think it will look better if the red bricks aren't together."])
                    self.wrong_said = True
                    self.state_entry = True
            elif bool(int(self.latest_conn_vec_msg[6])):
                if not self.wrong_said:
                    self.action_queue.insert(0, ["say", "That's another way to make square, but I think it will look better if the green bricks aren't together."])
                    self.wrong_said = True
                    self.state_entry = True
            elif bool(int(self.latest_conn_vec_msg[7])):
                if not self.wrong_said:
                    self.action_queue.insert(0, ["say", "That's another way to make square, but I think it will look better if the blue bricks aren't together."])
                    self.wrong_said = True
                    self.state_entry = True
            if self.count == self.max_counts:
                self.state_entry = True
                self.wrong_said = False
                

    def state_second_connection(self):
        if self.state_entry:
            time.sleep(self.wait_time)
            self.state_entry = False
            self.action_queue.insert(0, ["point", self.latest_conn_list_msg.connections[1].part])
            self.action_queue.insert(0, ["say", "Connect the red brick"])
            self.action_queue.insert(0, ["point", self.latest_conn_list_msg.connections[1].relative_part])
            self.action_queue.insert(0, ["say", "to the green brick you just used."])
            self.action_queue.insert(0, ["return", 0])
            self.count = 0
        elif self.new_conn_vec_msg:
            self.new_conn_vec_msg = False
            self.count += 1
            if bool(int(self.latest_conn_vec_msg[1])):
                self.count = self.max_counts
                self.action_queue.insert(0, ["say", self.random_congratulatory_msg()])
                self.state_entry = True
                if not bool(int(self.latest_conn_vec_msg[2])):
                    self.current_state = 3
                elif not bool(int(self.latest_conn_vec_msg[3])):
                    self.current_state = 4
                elif not bool(int(self.latest_conn_vec_msg[4])):
                    self.current_state = 5
                else:
                    self.current_state = 6
            elif bool(int(self.latest_conn_vec_msg[5])):
                if not self.wrong_said:
                    self.action_queue.insert(0, ["say", "It looks like you've put the two red bricks together."])
                    self.wrong_said = True
                    self.state_entry = True
            elif bool(int(self.latest_conn_vec_msg[6])):
                if not self.wrong_said:
                    self.action_queue.insert(0, ["say", "That doesn't look right. The two green bricks shouldn't go together."])
                    self.wrong_said = True
                    self.state_entry = True
            if self.count == self.max_counts:
                self.state_entry = True
                self.wrong_said = False

    def state_third_connection(self):
        if self.state_entry:
            time.sleep(self.wait_time)
            self.state_entry = False
            self.action_queue.insert(0, ["point", self.latest_conn_list_msg.connections[2].part])
            self.action_queue.insert(0, ["say", "Connect that blue brick"])
            self.action_queue.insert(0, ["point", self.latest_conn_list_msg.connections[2].relative_part])
            self.action_queue.insert(0, ["say", "to the red brick you used before."])
            self.action_queue.insert(0, ["return", 0])
            self.count = 0
        elif self.new_conn_vec_msg:
            self.new_conn_vec_msg = False
            self.count += 1
            if bool(int(self.latest_conn_vec_msg[2])):
                self.count = self.max_counts
                self.action_queue.insert(0, ["say", self.random_congratulatory_msg()])
                self.state_entry = True
                if not bool(int(self.latest_conn_vec_msg[3])):
                    self.current_state = 4
                elif not bool(int(self.latest_conn_vec_msg[4])):
                    self.current_state = 5
                else:
                    self.current_state = 6
            elif bool(int(self.latest_conn_vec_msg[5])):
                if not self.wrong_said:
                    self.action_queue.insert(0, ["say", "It looks like you've connected the two red bricks together. Try using the blue brick instead."])
                    self.wrong_said = True
                    self.state_entry = True
            elif bool(int(self.latest_conn_vec_msg[7])):
                if not self.wrong_said:
                    self.action_queue.insert(0, ["say", "It looks like you've connected the two blue bricks."])
                    self.wrong_said = True
                    self.state_entry = True
            if self.count == self.max_counts:
                self.state_entry = True
                self.wrong_said = False

    def state_fourth_connection(self):
        if self.state_entry:
            time.sleep(self.wait_time)
            self.state_entry = False
            self.action_queue.insert(0, ["point", self.latest_conn_list_msg.connections[3].part])
            self.action_queue.insert(0, ["say", "Put the other green brick"])
            self.action_queue.insert(0, ["point", self.latest_conn_list_msg.connections[3].relative_part])
            self.action_queue.insert(0, ["say", "into that blue brick."])
            self.action_queue.insert(0, ["return", 0])
            self.count = 0
        elif self.new_conn_vec_msg:
            self.new_conn_vec_msg = False
            self.count += 1
            if bool(int(self.latest_conn_vec_msg[3])):
                self.count = self.max_counts
                self.action_queue.insert(0, ["say", self.random_congratulatory_msg()])
                self.state_entry = True
                if not bool(int(self.latest_conn_vec_msg[4])):
                    self.current_state = 5
                else:
                    self.current_state = 6
            elif self.count == self.max_counts:
                self.state_entry = True
                self.wrong_said = False

    def state_fifth_connection(self):
        if self.state_entry:
            time.sleep(self.wait_time)
            self.state_entry = False
            self.action_queue.insert(0, ["point", self.latest_conn_list_msg.connections[4].part])
            self.action_queue.insert(0, ["say", "What about that last brick?"])
            self.action_queue.insert(0, ["return", 0])
            self.count = 0
        elif self.new_conn_vec_msg:
            self.new_conn_vec_msg = False
            self.count += 1
            if bool(int(self.latest_conn_vec_msg[4])):
                self.count = self.max_counts
                self.action_queue.insert(0, ["say", self.random_congratulatory_msg()])
                self.state_entry = True
                self.current_state = 6
            elif self.count == self.max_counts:
                self.state_entry = True
                self.wrong_said = False

    def state_final(self):
        if self.state_entry:
            time.sleep(self.wait_time)
            self.state_entry = False

    def run(self):
        if not self.action_queue:
            if self.current_state == 0:
                self.state_start()
            elif self.current_state == 1:
                self.state_first_connection()
            elif self.current_state == 2:
                self.state_second_connection()
            elif self.current_state == 3:
                self.state_third_connection()
            elif self.current_state == 4:
                self.state_fourth_connection()
            elif self.current_state == 5:
                self.state_fifth_connection()
            else:
                self.state_final()

    def get_action(self):
        if not self.action_queue:
            action, action_data = "none", 0
        else:
            action, action_data = self.action_queue.pop()
        return action, action_data
