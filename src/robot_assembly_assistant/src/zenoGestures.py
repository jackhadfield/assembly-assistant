import sys
import time
import math
import numpy

class ZenoGestures():
    def __init__(self):
        # TODO Initialize

    def say(self, sentence):
        # TODO Make Zeno say sentence

    def lookAndPoint(self, torsoObjectVector):
        # TODO Make Zeno look and point in direction of torsoObjectVector

    def returnToInit(self):
        # TODO Make Zeno return to initial state

if __name__ == '__main__':
    # Test code
    torsoObjectVector = [0.4, 0.2, -0.3]
    zenoGestures = ZenoGestures()
    zenoGestures.returnToInit()
    zenoGestures.lookAndPoint(torsoObjectVector)
    torsoObjectVector = [0.4, -0.2, -0.3]
    zenoGestures.say("Can you put this")
    zenoGestures.lookAndPoint(torsoObjectVector)
    zenoGestures.say("over here?")
    zenoGestures.returnToInit()

